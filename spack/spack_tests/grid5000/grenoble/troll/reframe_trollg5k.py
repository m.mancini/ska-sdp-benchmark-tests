"""
Troll - Grenoble - Grid5000
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The packages in ``spack/spack_tests/grid5000/grenoble/troll/configs/spack.yml`` can be installed
using following commands.

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c spack/spack_tests/grid5000/grenoble/troll/reframe_trollg5k.py --run

"""

import reframe as rfm

from modules.reframe_extras import SpackBase

# pylint: disable=C0116,E0401,E0602,E1101,W0201


@rfm.simple_test
class G5kTrollSpackEnv(SpackBase):
    """Test to create Spack env on troll cluster on Grid5000 at Grenoble site"""

    descr = 'Create Spack environment and install packages for troll cluster on Grid5000 at Grenoble'

    valid_systems = ['grenoble-g5k:troll-gcc9-ompi4-opa-smod']
    valid_prog_environs = ['builtin']
    maintainers = ['Mahendra Paipuri (mahendra.paipuri@inria.fr)']

    cluster = 'grenoble-g5k-troll'
    spack_root = '/home/mpaipuri/spack'

    time_limit = '0d3h00m0s'
