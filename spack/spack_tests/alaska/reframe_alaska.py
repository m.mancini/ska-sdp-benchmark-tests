"""
AlaSKA
~~~~~~~~~~~~~~~~

The packages in ``spack/spack_tests/alaska/configs/spack.yml`` can be installed using
following commands.

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c spack/spack_tests/alaska/reframe_alaska.py --run

"""

import reframe as rfm

from modules.reframe_extras import SpackBase

# pylint: disable=C0116,E0401,E0602,E1101,W0201


@rfm.simple_test
class AlaskaSpackEnv(SpackBase):
    """Test to create Spack env on AlaSKA Cluster"""

    descr = 'Create Spack environment and install packages for Alaska Cluster'

    valid_systems = ['alaska:login']
    valid_prog_environs = ['builtin']
    maintainers = ['Mahendra Paipuri (mahendra.paipuri@inria.fr)']

    cluster = 'alaska'
    spack_root = '/home/mahendra/spack'
    local = True
