import unittest
import modules.conda_env_manager as cem
import os
import logging

# INFO ERROR DEBUG WARNING
# logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger(__name__)


class CondaEnvManagerTest(unittest.TestCase):

    def test_create(self):
        logging.info("CondaEnvManagerTest")
        logging.info("--------------------\n")
        logging.info("test_create")
        test = cem.CondaEnvManager("test_create", False)
        test.remove()
        self.assertTrue(test.create())
        self.assertFalse(test.create())
        test.remove()

    def test_remove(self):
        logging.info("test_remove")
        test = cem.CondaEnvManager("test_remove", False)
        test.create()
        self.assertTrue(test.remove())
        os.mkdir(test.get_env_loc())
        self.assertTrue(test.remove())
        self.assertFalse(test.remove())

    def test_is_exist(self):
        logging.info("test_does_exist")
        test = cem.CondaEnvManager("test_does_exist", False)
        test.create()
        self.assertTrue(test.does_exist())
        test.remove()
        self.assertFalse(test.does_exist())

    def test_is_loaded(self):
        logging.info("test_is_loaded")
        test = cem.CondaEnvManager("test_is_loaded", False)
        test.create()
        self.assertTrue(test.is_loaded())
        test.remove()
        self.assertFalse(test.is_loaded())

    def test_install_conda(self):
        logging.info("test_install_conda_package")
        test = cem.CondaEnvManager("test_install_conda_package", False)
        test.create()
        self.assertTrue(test.install_conda("zipp"))
        self.assertFalse(test.install_conda("zzzzzz"))
        test.remove()

    def test_install_pip(self):
        logging.info("test_install_pip_package")
        test = cem.CondaEnvManager("test_install_pip_package", False)
        test.create()
        self.assertTrue(test.install_pip("zipp"))
        self.assertFalse(test.install_pip("zzzzzz"))
        test.remove()

    def test_download_install_local_pip(self):
        logging.info("test_download_install_local_pip_package")
        test = cem.CondaEnvManager("test_download_install_local_pip_package", False)
        test.create()
        self.assertTrue(test.download_pip("zipp"))
        self.assertFalse(test.download_pip("zzzzzz"))
        self.assertTrue(test.install_pip("zipp", True))
        self.assertFalse(test.install_pip("zzzzzz", True))
        self.assertTrue(test.purge_cache_pip())
        self.assertFalse(test.purge_cache_pip())
        test.remove()

    def test_install_download_local_pip_file(self):
        logging.info("test_install_download_local_pip_file")
        test = cem.CondaEnvManager("test_install_download_local_pip_file", False)
        test.create()
        root_dir = os.path.dirname(__file__)
        requirements_true = os.path.join(root_dir, 'resources', 'requirements.txt')
        requirement_false = os.path.join(root_dir, 'resources', 'requirements_false.txt')
        self.assertTrue(test.download_pip(requirements_true))
        self.assertFalse(test.download_pip(requirement_false))
        self.assertTrue(test.install_pip(requirements_true, True))
        self.assertFalse(test.install_pip(requirement_false, True))
        self.assertTrue(test.purge_cache_pip())
        self.assertFalse(test.purge_cache_pip())
        test.remove()
