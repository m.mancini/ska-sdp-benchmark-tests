""""This file contains the configuration of JUWELS cluster and its partitions"""

import os
from pathlib import Path
import json

# Get root dir
root_dir = Path(__file__).parent.parent.parent

# Load topology files
juwels_login_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'juwels-login.json'), 'r')
)
juwels_batch_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'juwels-batch.json'), 'r')
)
juwels_mem192_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'juwels-mem192.json'), 'r')
)


juwels_cluster_system_config = {
    'name': 'juwels-cluster',
    'descr': 'JUWELS Cluster is hosted at JSC, Leipzig with following config: '
             'Https://apps.fz-juelich.de/jsc/hps/juwels/configuration.html',
    'hostnames': ['jwlogin0(.*)', 'jwc*'],
    'modules_system': 'lmod',
    'partitions': [
        {
            'name': 'cluster-login',
            'descr': 'Login nodes for JUWELS Cluster',
            'scheduler': 'local',
            'launcher': 'local',
            'environs': ['builtin', 'gnu'],
            'processor': {
                **juwels_login_topo,
            },
            'prepare_cmds': [
                'source $HOME/.bashrc',
                'module --force unload Stages/2020 StdEnv/2020',
                'module purge',
            ],
        },  # <end login node>
        {
            'name': 'batch-gcc9-ompi4-ib-smod',
            'descr': 'This partition uses the gcc 9.3.0 and openmpi 4.1.0 '
                     'provided on the batch partition of JUWELS cluster',
            'scheduler': 'slurm',
            'launcher': 'srun',
            'time_limit': '0d2h0m0s',
            'access': [
                '--partition=batch',
                '--account=prpb106',
            ],
            'environs': [
                'builtin',
                'imaging-iotest',
                'imaging-iotest-mkl',
                'rascil',
            ],
            'modules': [
                'GCC/9.3.0', 'git/2.28.0',
                'git-lfs/2.12.0',
                'OpenMPI/4.1.0rc1',
            ],
            'variables': [
                ['SCRATCH_DIR', '/p/scratch/prpb106'],
                ['UCX_NET_DEVICES', 'mlx5_0:1'],
            ],
            'processor': {
                **juwels_batch_topo,
            },
            'prepare_cmds': [
                'source $HOME/.bashrc',
                'module purge',
                # 'srun () { command srun --label \"$@\"; }',  # wrap srun with
                # rank tag
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '96000000000',  # total memory in bytes
            },
        },  # <end batch partiton gcc/openmpi>
        {
            'name': 'batch-icc20-pmpi5-ib-smod',
            'descr': 'This partition uses the intel 2020.2 compiler and '
                     'parastation MPI 5.4 on the batch partition of JUWELS '
                     'cluster',
            'scheduler': 'slurm',
            'launcher': 'srun',
            'time_limit': '0d2h0m0s',
            'access': [
                '--partition=batch',
                '--account=prpb106',
            ],
            'environs': [
                'builtin',
                'imaging-iotest',
                'imaging-iotest-mkl',
            ],
            'modules': [
                'Intel/2020.2.254-GCC-9.3.0', 'git/2.28.0',
                'git-lfs/2.12.0',
                'ParaStationMPI/5.4.7-1-mt',
            ],
            'variables': [
                ['SCRATCH_DIR', '/p/scratch/prpb106'],
                ['UCX_NET_DEVICES', 'mlx5_0:1'],
            ],
            'processor': {
                **juwels_batch_topo,
            },
            'prepare_cmds': [
                'source $HOME/.bashrc',
                'module purge',
                # 'srun () { command srun --label \"$@\"; }',  # wrap srun with
                # rank tag
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '96000000000',  # total memory in bytes
            },
        },  # <end batch partition intel/parastation mpi>
        {
            'name': 'batch-gcc9-ompi4-ib-smod-mem192',
            'descr': 'This partition uses the gcc 9.3.0 and openmpi 4.1.0 '
                     'provided on the mem192 (high memory nodes 192 GiB) '
                     'partition of JUWELS cluster',
            'scheduler': 'slurm',
            'launcher': 'srun',
            'time_limit': '0d2h0m0s',
            'access': [
                '--partition=mem192',
                '--account=prpb106',
            ],
            'environs': [
                'builtin',
                'imaging-iotest',
                'imaging-iotest-mkl',
                'rascil',
            ],
            'modules': [
                'GCC/9.3.0', 'git/2.28.0',
                'git-lfs/2.12.0',
                'OpenMPI/4.1.0rc1',
            ],
            'variables': [
                ['SCRATCH_DIR', '/p/scratch/prpb106'],
                ['UCX_NET_DEVICES', 'mlx5_0:1'],
            ],
            'processor': {
                **juwels_mem192_topo,
            },
            'prepare_cmds': [
                'source $HOME/.bashrc',
                'module purge',
                # 'srun () { command srun --label \"$@\"; }',  # wrap srun with
                # rank tag
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '96000000000',  # total memory in bytes
            },
        },   # <end mem192 partition gcc/openmpi4>
        {
            'name': 'batch-icc20-pmpi5-ib-smod-mem192',
            'descr': 'This partition uses the intel 2020.2 compiler and '
                     'parastation MPI 5.4 on the mem192 '
                     '(high memory nodes 192 GiB) partition of JUWELS '
                     'cluster',
            'scheduler': 'slurm',
            'launcher': 'srun',
            'time_limit': '0d2h0m0s',
            'access': [
                '--partition=mem192',
                '--account=prpb106',
            ],
            'environs': [
                'builtin',
                'imaging-iotest',
                'imaging-iotest-mkl',
            ],
            'modules': [
                'Intel/2020.2.254-GCC-9.3.0', 'git/2.28.0',
                'git-lfs/2.12.0',
                'ParaStationMPI/5.4.7-1-mt',
            ],
            'variables': [
                ['SCRATCH_DIR', '/p/scratch/prpb106'],
                ['UCX_NET_DEVICES', 'mlx5_0:1'],
            ],
            'processor': {
                **juwels_mem192_topo,
            },
            'prepare_cmds': [
                'source $HOME/.bashrc',
                'module purge',
                # 'srun () { command srun --label \"$@\"; }',  # wrap srun with
                # rank tag
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '96000000000',  # total memory in bytes
            },
        },  # <end mem192 partition intel/parastation mpi>
        {
            'name': 'batch-gcc9-ompi4-ib-umod',
            'descr': 'This partition uses the gcc 9.3.0 and openmpi 4.1.1 '
                     'installed using Spack in user space',
            'scheduler': 'slurm',
            'launcher': 'mpirun',
            'time_limit': '0d2h0m0s',
            'access': [
                '--partition=batch',
                '--account=prpb106',
            ],
            'environs': [
                'builtin',
                'imaging-iotest',
                'imaging-iotest-mkl',
                'imb',
                'ior',
                'numpy',
            ],
            'modules': [
                'gcc/9.3.0',
                'git/2.31.1',
                'git-lfs/2.11.0',
                'openmpi/4.1.1',
            ],
            'variables': [
                ['SCRATCH_DIR', '/p/scratch/prpb106'],
                ['UCX_NET_DEVICES', 'mlx5_0:1'],
            ],
            'processor': {
                **juwels_batch_topo,
            },
            'prepare_cmds': [
                'source $HOME/.bashrc',
                'module --force unload Stages/2020 StdEnv/2020',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/juwels-cluster/lmod/linux*/Core',
                # 'mpirun () { command mpirun --tag-output --timestamp-output '
                # '\"$@\"; }',  # wrap mpirun output tag and timestamp
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '96000000000',  # total memory in bytes
            },
        },  # <end batch partiton spack gcc/openmpi>
        {
            'name': 'batch-icc21-impi21-ib-umod',
            'descr': 'This partition uses ICC 2021.4.0 intel MPI 2021.4.0 using '
                     'Spack on the batch partition of JUWELS cluster',
            'scheduler': 'slurm',
            'launcher': 'mpiexec',
            'time_limit': '0d2h0m0s',
            'access': [
                '--partition=batch',
                '--account=prpb106',
            ],
            'environs': [
                'babel-stream-tbb',
                'builtin',
                'imaging-iotest',
                'imaging-iotest-mkl',
                'intel-hpcg',
                'intel-hpl',
                'intel-stream',
            ],
            'modules': [
                'intel-oneapi-compilers/2021.4.0', ' git/2.31.1',
                'git-lfs/2.11.0', 'intel-oneapi-mpi/2021.4.0',
            ],
            'variables': [
                ['SCRATCH_DIR', '/p/scratch/prpb106'],
                ['UCX_NET_DEVICES', 'mlx5_0:1'],
            ],
            'processor': {
                **juwels_batch_topo,
            },
            'prepare_cmds': [
                'source $HOME/.bashrc',
                'module --force unload Stages/2020 StdEnv/2020',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/juwels-cluster/lmod/linux*/Core',
                # 'mpiexec () { command mpiexec -prepend-pattern \"[%r]: \" '
                # '\"$@\"; }',  # wrap mpirun with rank tag (intel mpi specific)
                'module purge',
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '96000000000',  # total memory in bytes
            },
        },  # <end batch partition spack gcc/intel mpi>
        {
            'name': 'batch-gcc9-ompi4-ib-umod-mem192',
            'descr': 'This partition uses the gcc 9.3.0 and openmpi 4.1.1 '
                     'installed by Spack on the mem192 (high memory nodes 192 GiB) '
                     'partition of JUWELS cluster',
            'scheduler': 'slurm',
            'launcher': 'mpirun',
            'time_limit': '0d2h0m0s',
            'access': [
                '--partition=mem192',
                '--account=prpb106',
            ],
            'environs': [
                'builtin',
                'imaging-iotest',
                'imaging-iotest-mkl',
            ],
            'modules': [
                'gcc/9.3.0',
                'git/2.31.1',
                'git-lfs/2.11.0',
                'openmpi/4.1.1',
            ],
            'variables': [
                ['SCRATCH_DIR', '/p/scratch/prpb106'],
                ['UCX_NET_DEVICES', 'mlx5_0:1'],
            ],
            'processor': {
                **juwels_mem192_topo,
            },
            'prepare_cmds': [
                'source $HOME/.bashrc',
                'module --force unload Stages/2020 StdEnv/2020',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/juwels-cluster/lmod/linux*/Core',
                # 'mpirun () { command mpirun --tag-output --timestamp-output '
                # '\"$@\"; }',  # wrap mpirun output tag and timestamp
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '96000000000',  # total memory in bytes
            },
        },   # <end mem192 partition gcc/openmpi4>
        {
            'name': 'batch-icc21-impi21-ib-umod-mem192',
            'descr': 'This partition uses the ICC 2021.4.0 and Intel MPI 2021.4.0 '
                     'installed by Spack on the mem192 (high memory nodes 192 GiB) partition of '
                     'JUWELS cluster',
            'scheduler': 'slurm',
            'launcher': 'mpiexec',
            'time_limit': '0d2h0m0s',
            'access': [
                '--partition=mem192',
                '--account=prpb106',
            ],
            'environs': [
                'builtin',
                'imaging-iotest',
                'imaging-iotest-mkl',
            ],
            'modules': [
                'intel-oneapi-compilers/2021.4.0', ' git/2.31.1',
                'git-lfs/2.11.0', 'intel-oneapi-mpi/2021.4.0',
            ],
            'variables': [
                ['SCRATCH_DIR', '/p/scratch/prpb106'],
                ['UCX_NET_DEVICES', 'mlx5_0:1'],
            ],
            'processor': {
                **juwels_mem192_topo,
            },
            'prepare_cmds': [
                'source $HOME/.bashrc',
                'module --force unload Stages/2020 StdEnv/2020',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/juwels-cluster/lmod/linux*/Core',
                # 'mpiexec () { command mpiexec -prepend-pattern \"[%r]: \" '
                # '\"$@\"; }',  # wrap mpirun with rank tag (intel mpi specific)
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '96000000000',  # total memory in bytes
            },
        },  # <end mem192 partition spack gcc/intel mpi>
        # <end JUWELS cluster partitions>
    ]
}
