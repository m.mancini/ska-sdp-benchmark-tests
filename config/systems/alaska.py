""""This file contains the configuration of AlaSKA and their partitions"""

import os
from pathlib import Path
import json

# Get root dir
root_dir = Path(__file__).parent.parent.parent

# Load topology files
alaska_login_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'alaska-login.json'), 'r')
)
alaska_compute_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'alaska-compute.json'), 'r')
)

alaska_system_config = {
    'name': 'alaska',
    'descr': 'Default AlaSKA OpenHPC p3-appliances slurm cluster',
    'hostnames': ['alaska-login-0', 'alaska-compute'],
    'modules_system': 'lmod',
    'partitions': [
        {
            'name': 'login',
            'descr': 'Login node of AlaSKA OpenHPC cluster '
                     '(Intel Core Processor (Broadwell, IBRS))',
            'scheduler': 'local',
            'launcher': 'local',
            'environs': [
                'builtin', 'gnu',
            ],
            'processor': {
                **alaska_login_topo,
            },
            'prepare_cmds': [
                'module purge',
            ],
            'extras': {
                'interconnect': '25',  # in Gb/s
            },
        },
        {
            'name': 'compute-gcc9-ompi4-roce-umod',
            'descr': 'AlaSKA OpenHPC cluster with 25Gb/s RoCE with gcc 9.3.0, openmpi 4.1.1 and '
                     'UCX transport layer (Intel Core Processor (Broadwell, IBRS))',
            'scheduler': 'slurm',
            'launcher': 'mpirun',
            'time_limit': '0d8h0m0s',
            'access': [
                '--partition=full',
                '--exclusive',
            ],
            'max_jobs': 8,
            'environs': [
                'babel-stream-omp',
                'builtin',
                'ior',
                'imaging-iotest',
                'imaging-iotest-mkl',
                'imb',
                'numpy',
                'rascil',
            ],
            'modules':  [
                'gcc/9.3.0', 'git/2.31.1',
                'git-lfs/2.11.0', 'openmpi/4.1.1'
            ],
            'variables': [
                # scratch dir
                ['SCRATCH_DIR', '/scratch/mahendra'],
                # use RoCE 25 Gb/s
                ['UCX_NET_DEVICES', 'mlx5_0:1'],
                # UCX likes to spit out tons of warnings. Confine log to errors
                ['UCX_LOG_LEVEL', 'ERROR'],
                # Set locale
                ['LC_ALL', 'en_US.UTF-8'],
            ],
            'processor': {
                **alaska_compute_topo,
            },
            'prepare_cmds': [
                # 'mpirun () { command mpirun --tag-output --timestamp-output '
                # '\"$@\"; }',  # wrap mpirun output tag and timestamp
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/alaska/lmod/linux*/Core',
            ],
            'extras': {
                'interconnect': '25',  # in Gb/s
                'mem': '115234864000',  # total memory in bytes
            },
        },
        {
            'name': 'compute-icc21-impi21-roce-umod',
            'descr': 'AlaSKA OpenHPC cluster with 25Gb/s RoCE with ICC 2021.4.0, '
                     'Intel-MPI 2021.4.0 (Intel Core Processor (Broadwell, IBRS))',
            'scheduler': 'slurm',
            'launcher': 'mpiexec',
            'time_limit': '0d8h0m0s',
            'access': [
                '--partition=full',
                '--exclusive',
            ],
            'max_jobs': 8,
            'environs': [
                'babel-stream-tbb',
                'builtin',
                'intel-hpcg',
                'intel-hpl',
                'imaging-iotest',
                'imaging-iotest-mkl',
                'intel-stream',
            ],
            'modules':  [
                'intel-oneapi-compilers/2021.4.0', ' git/2.31.1',
                'git-lfs/2.11.0', 'intel-oneapi-mpi/2021.4.0',
            ],
            'variables': [
                # scratch dir
                ['SCRATCH_DIR', '/scratch/mahendra'],
                # # use ib (default) https://software.intel.com/content/www/us/en/develop/articles/intel-mpi-library-2019-over-libfabric.html
                # ['FI_VERBS_IFACE', 'ib'],
                # Set locale
                ['LC_ALL', 'en_US.UTF-8'],
            ],
            'processor': {
                **alaska_compute_topo,
            },
            'prepare_cmds': [
                # 'mpiexec () { command mpiexec -prepend-pattern \"[%r]: \" '
                # '\"$@\"; }',  # wrap mpirun with rank tag (intel mpi specific)
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/alaska/lmod/linux*/Core',
            ],
            'extras': {
                'interconnect': '25',  # in Gb/s
                'mem': '115234864000',  # total memory in bytes
            },
        },
    ]
}
