""""This file contains the configuration for GitLab CI runner with Kubernetes executor of SKA"""

import os
from pathlib import Path
import json

# Get root dir
root_dir = Path(__file__).parent.parent.parent

# Load topology files

runner_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'gitlab-runner.json'), 'r')
)

ci_runner_config = {
    'name': 'gitlab-ci',
    'descr': 'GitLab CI system configuration',
    'hostnames': ['runner-.*'],
    'modules_system': 'nomod',
    'partitions': [
        {
            'name': 'k8-runner',
            'descr': 'Gitlab CI runners based on Kubernetes executor',
            'scheduler': 'local',
            'launcher': 'mpirun',
            'environs': [
                'ci-environ', 'cng-test',
                'gnu', 'idg-test',
            ],
            'processor': {
                **runner_topo,
            },
            'variables': [
                ['SCRATCH_DIR', '/tmp/'],
            ],
            'devices': [
                {
                    'type': 'gpu',
                    'arch': '61',
                    'num_devices': 1,
                },
            ],
            'prepare_cmds': [
                'mpirun () { command mpirun --allow-run-as-root '
                '\"$@\"; }',  # wrap mpirun allow run as root arg
                'source $HOME/.bashrc',
            ],
            'extras': {
                'interconnect': '10',  # in Gb/s
                'mem': '82480868000',  # total memory in bytes
            },
        },
    ]
}
