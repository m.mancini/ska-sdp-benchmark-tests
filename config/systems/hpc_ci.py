""""This file contains the configuration for Jacamar HPC CI runner"""

import os
from pathlib import Path
import json

# Get root dir
root_dir = Path(__file__).parent.parent.parent

# Load topology files
# Both login and compute nodes of this cluster has same topology
grvingt_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'grvingt-grid5000.json'), 'r')
)

hpc_ci_config = {
    'name': 'jacamar-hpc-ci',
    'descr': 'HPC CI runner based on SLURM Cluster deployed using Jacamar CI. This cluster '
             'is deployed on Grvingt cluster on Grid5000',
    'hostnames': ['grvingt-.*'],
    'modules_system': 'lmod',
    'partitions': [
        {
            'name': 'login',
            'descr': 'Grvingt SLURM cluster at the nancy site with 100 Gbps Omnipath, '
                     '(2 x Intel Xeon Gold 6130) Login node '
                     'https://www.grid5000.fr/w/Nancy:Hardware#grvingt',
            'scheduler': 'local',
            'launcher': 'local',
            'environs': [
                'builtin', 'gnu',
            ],
            'processor': {
                **grvingt_topo,
            },
            'prepare_cmds': [
                'source $HOME/.bashrc',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/jacamar-hpc-ci-grvingt/lmod/linux*/Core',
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '192000000000',  # total memory in bytes
            },
        },
        {
            'name': 'grvingt-gcc9-ompi4-opa-umod',
            'descr': 'Grvingt SLURM cluster at the nancy site with 100 Gbps Omnipath, '
                     'GCC 9.3.0 and OpenMPI 4.1.1 (2 x Intel Xeon Gold 6130) '
                     'https://www.grid5000.fr/w/Nancy:Hardware#grvingt',
            'scheduler': 'slurm',
            'launcher': 'mpirun',
            'time_limit': '0d4h0m0s',
            'access': [
                '--partition skasdpci',
                '--overcommit',
            ],
            'environs': [
                'builtin', 'gnu',
                'imaging-iotest',
                'imaging-iotest-mkl',
                'rascil',
            ],
            'variables': [
                # No scratch dir on this partition
                ['SCRATCH_DIR', '/tmp/'],
            ],
            'modules': [
                'gcc/9.3.0', ' git/2.31.1',
                'git-lfs/2.11.0', 'openmpi/4.1.1',
            ],
            'processor': {
                **grvingt_topo,
            },
            'prepare_cmds': [
                'source $HOME/.bashrc',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/jacamar-hpc-ci-grvingt/lmod/linux*/Core',
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '192000000000',  # total memory in bytes
            },
        },
        {
            'name': 'grvingt-icc21-impi21-opa-umod',
            'descr': 'Grvingt SLURM cluster at the nancy site with 100 Gbps Omnipath, '
                     'ICC 2021.4.0 and Intel MPI 2021.4.0 (2 x Intel Xeon Gold 6130) '
                     'https://www.grid5000.fr/w/Nancy:Hardware#grvingt',
            'scheduler': 'slurm',
            'launcher': 'mpiexec',
            'time_limit': '0d4h0m0s',
            'access': [
                '--partition skasdpci',
                '--overcommit',
            ],
            'environs': [
                'builtin', 'gnu',
                'imaging-iotest',
                'imaging-iotest-mkl',
            ],
            'variables': [
                # No scratch dir on this partition
                ['SCRATCH_DIR', '/tmp/'],
            ],
            'modules': [
                'intel-oneapi-compilers/2021.4.0', ' git/2.31.1',
                'git-lfs/2.11.0', 'intel-oneapi-mpi/2021.4.0',
            ],
            'processor': {
                **grvingt_topo,
            },
            'prepare_cmds': [
                'source $HOME/.bashrc',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/jacamar-hpc-ci-grvingt/lmod/linux*/Core',
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '192000000000',  # total memory in bytes
            },
        },  # <end jacamar hpc ci grvingt partition>
    ]
}
