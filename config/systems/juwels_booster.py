""""This file contains the configuration of JUWELS Booster and its partitions"""

import os
from pathlib import Path
import json

# Get root dir
root_dir = Path(__file__).parent.parent.parent

# Load topology files
booster_login_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'juwels-booster-login.json'), 'r')
)
juwels_booster_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'juwels-booster.json'), 'r')
)

juwels_booster_system_config = {
    'name': 'juwels-booster',
    'descr': 'JUWELS Booster is hosted at JSC, Leipzig with following config: '
             'Https://apps.fz-juelich.de/jsc/hps/juwels/configuration.html',
    'hostnames': ['jwlogin2(.*)', 'jwcb*'],
    'modules_system': 'lmod',
    'partitions': [
        {
            'name': 'booster-login',
            'descr': 'Login nodes for JUWELS Booster Cluster',
            'scheduler': 'local',
            'launcher': 'local',
            'environs': ['builtin', 'gnu'],
            'processor': {
                **booster_login_topo,
            },
            'prepare_cmds': [
                'source $HOME/.bashrc',
                'module --force unload Stages/2020 StdEnv/2020',
                'module purge',
            ],
        },  # <end login node>
        {
            'name': 'booster-gcc9-ompi4-ib-smod-nvgpu',
            'descr': 'This partition uses the gcc 9.3.0, cuda 11.4.0 and openmpi 4.1.0 '
                     'provided on the booster partition of JUWELS cluster',
            'scheduler': 'slurm',
            'launcher': 'srun',
            'time_limit': '0d2h0m0s',
            'access': [
                '--partition=booster',
                '--account=prpb106',
                '--gres=gpu:4',
            ],
            'environs': [
                'builtin',
            ],
            'modules': [
                'GCC/9.3.0', 'git/2.28.0',
                'git-lfs/2.12.0', 'CUDA/11.3',
                'OpenMPI/4.1.0rc1',
            ],
            'variables': [
                ['SCRATCH_DIR', '/p/scratch/prpb106'],
                ['UCX_NET_DEVICES', 'mlx5_0:1'],
            ],
            'processor': {
                **juwels_booster_topo,
            },
            'devices': [
                {
                    'type': 'gpu',
                    'arch': '80',
                    'num_devices': 4,
                },
            ],
            'prepare_cmds': [
                'source $HOME/.bashrc',
                'module purge',
                # 'srun () { command srun --label \"$@\"; }',  # wrap srun with rank tag
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '512000000000',  # total memory in bytes
                'gpu_mem': '42505076736',  # in bytes
                'gdr_test_net_adptr': 'mlx5_0:1',  # NIC that has end-to-end connectivity for GDR test
            },
        },  # <end booster partiton gcc/openmpi>
        {
            'name': 'booster-gcc9-ompi4-ib-smod',
            'descr': 'This partition uses the gcc 9.3.0, cuda 11.4.0 and openmpi 4.1.0 '
                     'provided on the booster partition of JUWELS cluster WITHOUT GPUs',
            'scheduler': 'slurm',
            'launcher': 'srun',
            'time_limit': '0d2h0m0s',
            'access': [
                '--partition=booster',
                '--account=prpb106',
                '--gres=gpu:0',
            ],
            'environs': [
                'builtin',
            ],
            'modules': [
                'GCC/9.3.0', 'git/2.28.0',
                'git-lfs/2.12.0',
                'OpenMPI/4.1.0rc1',
            ],
            'variables': [
                ['SCRATCH_DIR', '/p/scratch/prpb106'],
                ['UCX_NET_DEVICES', 'mlx5_0:1'],
            ],
            'processor': {
                **juwels_booster_topo,
            },
            'prepare_cmds': [
                'source $HOME/.bashrc',
                'module purge',
                # 'srun () { command srun --label \"$@\"; }',  # wrap srun with rank tag
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '512000000000',  # total memory in bytes
            },
        },  # <end booster partition gcc/openmpi WITHOUT GPUs>
        {
            'name': 'booster-gcc9-ompi4-ib-umod-nvgpu',
            'descr': 'This partition uses the gcc 9.3.0, cuda 11.3.0 and openmpi 4.1.1 '
                     'build by Spack on the booster partition of JUWELS cluster',
            'scheduler': 'slurm',
            'launcher': 'mpirun',
            'time_limit': '0d2h0m0s',
            'access': [
                '--partition=booster',
                '--account=prpb106',
                '--gres=gpu:4',
            ],
            'environs': [
                'babel-stream-cuda',
                'builtin',
                'gpu-direct-rdma',
                'nccl-test',
            ],
            'modules': [
                'gcc/9.3.0', 'git/2.31.1',
                'git-lfs/2.11.0', 'cuda/11.4.0',
                'openmpi/4.1.1-cuda-11.4.0',
            ],
            'variables': [
                ['SCRATCH_DIR', '/p/scratch/prpb106'],
                ['UCX_NET_DEVICES', 'mlx5_0:1'],
            ],
            'processor': {
                **juwels_booster_topo,
            },
            'devices': [
                {
                    'type': 'gpu',
                    'arch': '80',
                    'num_devices': 4,
                },
            ],
            'prepare_cmds': [
                'source $HOME/.bashrc',
                'module --force unload Stages/2020 StdEnv/2020',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/juwels-booster/lmod/linux*/Core',
                # 'mpirun () { command mpirun --tag-output --timestamp-output '
                # '\"$@\"; }',  # wrap mpirun output tag and timestamp
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '512000000000',  # total memory in bytes
                'gpu_mem': '42505076736',  # in bytes
                'gdr_test_net_adptr': 'mlx5_0:1',  # NIC that has end-to-end connectivity for GDR test
            },
        },  # <end booster partition spack gcc/openmpi>
        {
            'name': 'booster-gcc9-ompi4-ib-umod',
            'descr': 'This partition uses the gcc 9.3.0, cuda 11.3.0 and openmpi 4.1.1 '
                     'build by Spack on the booster partition of JUWELS cluster WITHOUT GPUs',
            'scheduler': 'slurm',
            'launcher': 'mpirun',
            'time_limit': '0d2h0m0s',
            'access': [
                '--partition=booster',
                '--account=prpb106',
                '--gres=gpu:0',
            ],
            'environs': [
                'babel-stream-omp',
                'builtin',
                'gnu-hpcg',
                'gnu-hpl',
                'gnu-stream'
            ],
            'modules': [
                'gcc/9.3.0', 'git/2.31.1',
                'git-lfs/2.11.0',
                'openmpi/4.1.1-cuda-11.4.0',
            ],
            'variables': [
                ['SCRATCH_DIR', '/p/scratch/prpb106'],
                ['UCX_NET_DEVICES', 'mlx5_0:1'],
            ],
            'processor': {
                **juwels_booster_topo,
            },
            'prepare_cmds': [
                'source $HOME/.bashrc',
                'module --force unload Stages/2020 StdEnv/2020',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/juwels-booster/lmod/linux*/Core',
                # 'mpirun () { command mpirun --tag-output --timestamp-output '
                # '\"$@\"; }',  # wrap mpirun output tag and timestamp
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '512000000000',  # total memory in bytes
            },
        },  # <end booster partition spack gcc/openmpi>
    ]
}
