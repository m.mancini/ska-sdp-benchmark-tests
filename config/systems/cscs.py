""""This file contains the configuration of CSCS and their partitions"""

import os
from pathlib import Path
import json

# Get root dir
root_dir = Path(__file__).parent.parent.parent

# Load topology files
cscs_login_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'cscs-login.json'), 'r')
)
cscs_compute_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'cscs-compute.json'), 'r')
)
cscs_system_config = {
    'name': 'cscs-daint',
    'descr': 'Piz Daint cluster at the Swiss National Supercomputing Centre (CSCS)',
    'hostnames': ['daint'],
    'modules_system': 'tmod',
    'partitions': [
        {
            'name': 'login',
            'descr': 'Login node of Piz Daint @ CSCS',
            'scheduler': 'local',
            'launcher': 'local',
            'environs': [
                'builtin',
                'gnu'
            ],
            'processor': {
                **cscs_login_topo,
            },
            'max_jobs': 4,
            'modules': [
                'gcc/9.3.0',
                'cray-mpich'
            ],
        },
        {
            'name': 'daint-gcc9-ompi4-ib-umod-gpu',
            'descr': 'CSCS PizDaint GPU node',
            'scheduler': 'slurm',
            'launcher': 'srun',
            'container_platforms': [
                {
                    'type': 'Sarus',
                    'modules': ['sarus']
                },
                {
                    'type': 'Singularity',
                    'modules': ['singularity']
                }
            ],
            'time_limit': '1d0h0m0s',
            'access': [
                # Constrained to Hybrid GPU nodes
                '--constraint="gpu&perf"',

                # By default, use the "normal" slurm queue (2400 nodes / 24h time limit)
                '--partition=normal',
                '--exclusive',

                # Scoop account on CSCS. Contact Manuel Stutz <manuel.stutz@fhnw.ch> for details.
                '--account=sk04',
                
                # num_cores yields 24, but those are only usable with 2 tasks per core.
                # Can be overwritten on a per-test basis
                '--ntasks-per-core=2'
            ],
            'max_jobs': 100,
            'environs': [
                'builtin',
                'gnu',

                'numpy',
                'numpy_cuda',

                'gnu-hpcg',
                'gnu-stream',
                'cng-test',
                'babel-stream-omp',
                'babel-stream-cuda',
                'babel-stream-tbb',
                'idg-test',

                'funclib-test',

                'rascil',
            ],
            'modules': [
                'daint-gpu',
                'gcc/9.3.0',
                'cray-mpich',
                'perftools-base'
            ],
            'processor': {
                **cscs_compute_topo,
            },
            'prepare_cmds': [
            ],
            'extras': {
                'mem': '67317911552',  # total memory in bytes
                'gpu_mem': '17070817280',
                'internet': True
            },
            'devices': [
                {
                    'type': 'gpu',
                    'arch': '60',  # compute capability of 6.0 (https://developer.nvidia.com/cuda-gpus)
                    'num_devices': 1,
                },
            ],
        },

        {
            'name': 'daint-icc21-impi21-ib-umod-gpu',
            'descr': 'CSCS PizDaint GPU node with Intel toolchain',
            'scheduler': 'slurm',
            'launcher': 'srun',
            'time_limit': '1d0h0m0s',
            'access': [
                # Constrained to Hybrid GPU nodes
                '--constraint="gpu&perf"',

                # By default, use the "normal" slurm queue (2400 nodes / 24h time limit)
                '--partition=normal',
                '--exclusive',

                # Scoop account on CSCS. Contact Manuel Stutz <manuel.stutz@fhnw.ch> for details.
                '--account=sk04',

                # num_cores yields 24, but those are only usable with 2 tasks per core.
                # Can be overwritten on a per-test basis
                '--ntasks-per-core=2'
            ],
            'max_jobs': 100,
            'environs': [  # todo
                'builtin',

                'intel-stream',

                'intel-hpcg',
                'intel-hpl',

                'nccl-test',
                'gpu-direct-rdma',

                # only gnu imaging iotest  work currently
                'imaging-iotest',
                'imaging-iotest-mkl',

                'imb',
                # ior do not work (missing libgpfs.so on compute nodes)
                'ior',
            ],
            'modules': [
                'intel/2022.1.0',
                'cray-mpich'
            ],
            'processor': {
                **cscs_compute_topo,
            },
            'prepare_cmds': [
            ],
            'extras': {
                'mem': '67317911552',  # total memory in bytes
                'gpu_mem': '17070817280',
                'internet': True
            },
            'devices': [
                {
                    'type': 'gpu',
                    'arch': '60',  # compute capability of 6.0 (https://developer.nvidia.com/cuda-gpus)
                    'num_devices': 1,
                },
            ],
        }
    ]
}

