""""This file contains the configuration of grenoble grid5000 and their partitions"""

import os
from pathlib import Path
import json

# Get root dir
root_dir = Path(__file__).parent.parent.parent

# Load topology files
dahu_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'dahu-grid5000.json'), 'r')
)
troll_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'troll-grid5000.json'), 'r')
)

grenoble_g5k_system_config = {
    'name': 'grenoble-g5k',
    'descr': 'Grid5000 is a large-scale and flexible testbed for experiment-driven '
             'research: https://www.grid5000.fr/w/Grid5000:Home. This is Grenoble '
             'partition: https://www.grid5000.fr/w/Grenoble:Hardware',
    'hostnames': ['fgrenoble', 'dahu-*'],
    'modules_system': 'lmod',
    'partitions': [
        {
            'name': 'login',
            'descr': 'Generic frontend node for grenoble clusters',
            'scheduler': 'local',
            'launcher': 'local',
            'environs': ['builtin', 'gnu'],
            'prepare_cmds': [
                'module purge',
            ],
        },
        {
            'name': 'dahu-gcc9-ompi4-opa-smod',
            'descr': 'Dahu cluster at the grenoble site with 100Gb Intel omni-path, '
                     'Generic configuration provided by g5k (2 x Intel Xeon '
                     'Gold 6130) '
                     'https://www.grid5000.fr/w/Grenoble:Hardware#dahu',
            'scheduler': 'oar',
            'launcher': 'mpirun',
            'time_limit': '0d8h0m0s',
            'access': [
                '-p cluster=\'dahu\'',
            ],
            'environs': [
                'builtin',
            ],
            'variables': [
                # specific for g5000 clusters
                ['OMPI_MCA_orte_rsh_agent', '\"oarsh\"'],
                ['SCRATCH_DIR', '/tmp/'],
            ],
            'processor': {
                **dahu_topo,
            },
            'prepare_cmds': [  # oar specific commands
                'source /etc/profile.d/lmod.sh',
                'source $HOME/.bashrc',
                'module purge',
                'mpirun () { command $(which mpirun) --hostfile $OAR_NODEFILE '
                '\"$@\"; }',  # wrap mpirun with hostfile arg
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '192000000000',  # total memory in bytes
            },
        },
        {
            'name': 'dahu-gcc9-ompi4-opa-umod',
            'descr': 'Dahu cluster at the grenoble site with 100Gb Intel omni-path, '
                     'gcc 9.3.0 and OpenMPI 4.1.1 with psm2 support (2 x Intel Xeon Gold 6130) '
                     'https://www.grid5000.fr/w/Grenoble:Hardware#dahu',
            'scheduler': 'oar',
            'launcher': 'mpirun',
            'time_limit': '0d8h0m0s',
            'access': [
                '-p cluster=\'dahu\'',
            ],
            'environs': [
                'babel-stream-omp',
                'builtin',
                'imaging-iotest',
                'imaging-iotest-mkl',
                'imb',
                'numpy',
                'rascil',
            ],
            'variables': [
                # specific for g5000 clusters
                ['SCRATCH_DIR', '/tmp/'],
            ],
            'modules': [
                'gcc/9.3.0', 'openmpi/4.1.1'
            ],
            'processor': {
                **dahu_topo,
            },
            'prepare_cmds': [  # oar specific commands
                'source /etc/profile.d/lmod.sh',
                'source $HOME/.bashrc',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/grenoble-g5k-dahu/lmod/linux*/Core',
                'mpirun () { command $(which mpirun) --hostfile $OAR_NODEFILE --mca orte_rsh_agent \"oarsh\" '
                '\"$@\"; }',  # wrap mpirun with hostfile arg
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '192000000000',  # total memory in bytes
            },
        },
        {
            'name': 'dahu-icc21-impi21-opa-umod',
            'descr': 'Dahu cluster at the grenoble site with 100Gb Intel omni-path, '
                     'ICC 2021.4.0 and Intel MPI 2021.4.0 '
                     '(2 x Intel Xeon Gold 6130) '
                     'https://www.grid5000.fr/w/Grenoble:Hardware#dahu',
            'scheduler': 'oar',
            'launcher': 'mpiexec',
            'time_limit': '0d8h0m0s',
            'access': [
                '-p cluster=\'dahu\'',
            ],
            'environs': [
                'babel-stream-tbb',
                'builtin',
                'intel-hpcg',
                'intel-hpl',
                'imaging-iotest',
                'imaging-iotest-mkl',
                'intel-stream',
            ],
            'variables': [
                # specific for g5000 clusters
                ['SCRATCH_DIR', '/tmp/'],
            ],
            'modules': [
                'intel-oneapi-compilers/2021.4.0', 'intel-oneapi-mpi/2021.4.0',
            ],
            'processor': {
                **dahu_topo,
            },
            'prepare_cmds': [  # oar specific commands
                'source /etc/profile.d/lmod.sh',
                'source $HOME/.bashrc',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/grenoble-g5k-dahu/lmod/linux*/Core',
                'mpiexec () { command $(which mpiexec) -genvall -f $OAR_NODEFILE '
                '-launcher ssh -launcher-exec /usr/bin/oarsh '
                '\"$@\"; }',  # wrap mpirun with hostfile arg
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '192000000000',  # total memory in bytes
            },
        },
        {
            'name': 'troll-gcc9-ompi4-opa-smod',
            'descr': 'Troll cluster at the grenoble site with 100Gb Intel omni-path, '
                     'Generic configuration provided by g5k (2 x Intel Xeon '
                     'Gold 5218) '
                     'https://www.grid5000.fr/w/Grenoble:Hardware#troll',
            'scheduler': 'oar',
            'launcher': 'mpirun',
            'time_limit': '0d8h0m0s',
            'access': [
                '-p cluster=\'troll\'',
                '-t exotic',
            ],
            'environs': [
                'builtin',
            ],
            'variables': [
                # specific for g5000 clusters
                ['OMPI_MCA_orte_rsh_agent', '\"oarsh\"'],
                ['SCRATCH_DIR', '/tmp/'],
            ],
            'processor': {
                **troll_topo,
            },
            'prepare_cmds': [  # oar specific commands
                'source /etc/profile.d/lmod.sh',
                'source $HOME/.bashrc',
                'module purge',
                'mpirun () { command $(which mpirun) --hostfile $OAR_NODEFILE '
                '\"$@\"; }',  # wrap mpirun with hostfile arg
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '1500000000000',  # total memory in bytes
            },
        },
        {
            'name': 'troll-gcc9-ompi4-opa-umod',
            'descr': 'Troll cluster at the grenoble site with 100Gb Intel omni-path, '
                     'gcc 9.3.0 and OpenMPI 4.1.1 with psm2 support (2 x Intel Xeon '
                     'Gold 5218) '
                     'https://www.grid5000.fr/w/Grenoble:Hardware#troll',
            'scheduler': 'oar',
            'launcher': 'mpirun',
            'time_limit': '0d8h0m0s',
            'access': [
                '-p cluster=\'troll\'',
                '-t exotic',
            ],
            'environs': [
                'builtin', 'rascil',
            ],
            'variables': [
                # specific for g5000 clusters
                ['OMPI_MCA_orte_rsh_agent', '\"oarsh\"'],
                ['SCRATCH_DIR', '/tmp/'],
            ],
            'modules': [
                'gcc/9.3.0', 'openmpi/4.1.1'
            ],
            'processor': {
                **troll_topo,
            },
            'prepare_cmds': [  # oar specific commands
                'source /etc/profile.d/lmod.sh',
                'source $HOME/.bashrc',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/grenoble-g5k-troll/lmod/linux*/Core',
                'mpirun () { command $(which mpirun) --hostfile $OAR_NODEFILE '
                '\"$@\"; }',  # wrap mpirun with hostfile arg
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '1500000000000',  # total memory in bytes
            },
        },
        {
            'name': 'troll-icc21-impi21-opa-umod',
            'descr': 'Troll cluster at the grenoble site with 100Gb Intel omni-path, '
                     'ICC 2021.4.0 and Intel MPI 2021.4.0 (2 x Intel Xeon '
                     'Gold 5218) '
                     'https://www.grid5000.fr/w/Grenoble:Hardware#troll',
            'scheduler': 'oar',
            'launcher': 'mpirun',
            'time_limit': '0d8h0m0s',
            'access': [
                '-p cluster=\'troll\'',
                '-t exotic',
            ],
            'environs': [
                'builtin',
            ],
            'variables': [
                # specific for g5000 clusters
                ['OMPI_MCA_orte_rsh_agent', '\"oarsh\"'],
                ['SCRATCH_DIR', '/tmp/'],
            ],
            'modules': [
                'intel-oneapi-compilers/2021.4.0', 'intel-oneapi-mpi/2021.4.0',
            ],
            'processor': {
                **troll_topo,
            },
            'prepare_cmds': [  # oar specific commands
                'source /etc/profile.d/lmod.sh',
                'source $HOME/.bashrc',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/grenoble-g5k-troll/lmod/linux*/Core',
                'mpirun () { command $(which mpirun) --hostfile $OAR_NODEFILE '
                '\"$@\"; }',  # wrap mpirun with hostfile arg
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '1500000000000',  # total memory in bytes
            },
        },
    ]
}
