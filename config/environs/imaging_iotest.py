""""This file contains the environment config for Imaging IO test benchmark"""


imaging_iotest_environ = [
    {
        'name': 'imaging-iotest',
        'target_systems': [
            'juwels-cluster:batch-gcc9-ompi4-ib-smod',
            'juwels-cluster:batch-gcc9-ompi4-ib-smod-mem192',
        ],
        'modules': [
            'HDF5/1.10.6', 'FFTW/3.3.8 ',
            'CMake/3.18.0',
        ],
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
    },
    {
        'name': 'imaging-iotest',
        'target_systems': [
            'juwels-cluster:batch-icc20-pmpi5-ib-smod',
            'juwels-cluster:batch-icc20-pmpi5-ib-smod-mem192',
        ],
        'modules': [
            'HDF5/1.10.6', 'FFTW/3.3.8 ',
            'CMake/3.18.0'
        ],
        'cc': 'mpiicc',
        'cxx': 'mpiicpc',
        'ftn': 'mpiifort',
    },  # <end JUWELS system software stack>
    {
        'name': 'imaging-iotest-mkl',
        'target_systems': [
            'juwels-cluster:batch-gcc9-ompi4-ib-smod',
            'juwels-cluster:batch-gcc9-ompi4-ib-smod-mem192',
        ],
        'modules': [
            'HDF5/1.10.6', 'FFTW/3.3.8 ',
            'CMake/3.18.0', 'imkl/2020.4.304',
        ],
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
    },
    {
        'name': 'imaging-iotest-mkl',
        'target_systems': [
            'juwels-cluster:batch-icc20-pmpi5-ib-smod',
            'juwels-cluster:batch-icc20-pmpi5-ib-smod-mem192',
        ],
        'modules': [
            'HDF5/1.10.6', 'FFTW/3.3.8 ',
            'imkl/2020.4.304', 'CMake/3.18.0'
        ],
        'cc': 'mpiicc',
        'cxx': 'mpiicpc',
        'ftn': 'mpiifort',
    },  # <end JUWELS system software stack with IMKL>
    {
        'name': 'imaging-iotest',
        'target_systems': [
            'alaska:compute-gcc9-ompi4-roce-umod',
            'grenoble-g5k:dahu-gcc9-ompi4-opa-umod',
            'jacamar-hpc-ci:grvingt-gcc9-ompi4-opa-umod',
            'juwels-cluster:batch-gcc9-ompi4-ib-umod',
            'juwels-cluster:batch-gcc9-ompi4-ib-umod-mem192',
            'nancy-g5k:gros-gcc9-ompi4-eth-umod',
        ],
        'modules': [
            'hdf5/1.10.7-openmpi-4.1.1', 'fftw/3.3.9', 'cmake/3.21.1',
        ],
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
    },
    {
        'name': 'imaging-iotest',
        'target_systems': [
            'alaska:compute-icc21-impi21-roce-umod',
            'grenoble-g5k:dahu-icc21-impi21-opa-umod',
            'jacamar-hpc-ci:grvingt-icc21-impi21-opa-umod',
            'juwels-cluster:batch-icc21-impi21-ib-umod',
            'juwels-cluster:batch-icc21-impi21-ib-umod-mem192',
            'nancy-g5k:gros-icc21-impi21-eth-umod',
        ],
        'modules': [
            'hdf5/1.10.7-intel-oneapi-mpi-2021.4.0', 'fftw/3.3.9', 'cmake/3.21.1',
        ],
        'cc': 'mpiicc',
        'cxx': 'mpiicpc',
        'ftn': 'mpiifort',
    },
    {
        'name': 'imaging-iotest-mkl',
        'target_systems': [
            'alaska:compute-gcc9-ompi4-roce-umod',
            'grenoble-g5k:dahu-gcc9-ompi4-opa-umod',
            'jacamar-hpc-ci:grvingt-gcc9-ompi4-opa-umod',
            'juwels-cluster:batch-gcc9-ompi4-ib-umod',
            'juwels-cluster:batch-gcc9-ompi4-ib-umod-mem192',
            'nancy-g5k:gros-gcc9-ompi4-eth-umod',
        ],
        'modules': [
            'hdf5/1.10.7-openmpi-4.1.1', 'fftw/3.3.9', 'cmake/3.21.1',
            'intel-oneapi-mkl/2021.3.0',
        ],
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
    },
    {
        'name': 'imaging-iotest-mkl',
        'target_systems': [
            'alaska:compute-icc21-impi21-roce-umod',
            'grenoble-g5k:dahu-icc21-impi21-opa-umod',
            'jacamar-hpc-ci:grvingt-icc21-impi21-opa-umod',
            'juwels-cluster:batch-icc21-impi21-ib-umod',
            'juwels-cluster:batch-icc21-impi21-ib-umod-mem192',
            'nancy-g5k:gros-icc21-impi21-eth-umod',
        ],
        'modules': [
            'hdf5/1.10.7-intel-oneapi-mpi-2021.4.0', 'fftw/3.3.9',
            'intel-oneapi-mkl/2021.3.0', 'cmake/3.21.1',
        ],
        'cc': 'mpiicc',
        'cxx': 'mpiicpc',
        'ftn': 'mpiifort',
    },
    {
        'name': 'imaging-iotest',
        'target_systems': [
            'jeanzay', 
            # For some reason ReFrame does not match these with the declarations in jeanzay.py
            # so revert to simple jeanzay
            # 'jeanzay:jeanzay-gcc8-ompi4-opa-smod-cpu-short',
            # 'jeanzay:jeanzay-gcc8-ompi4-opa-smod-cpu-long',
        ],
        'modules': [
            'git-lfs/3.3.0',
            'hdf5/1.12.0',
            'fftw/3.3.10',
            'cmake/3.21.3',
            'openmpi/4.1.5',
        ],
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        # 'cc': '/gpfswork/rech/nhx/commun/scorep-7/bin/scorep-mpicc',
        # 'cxx': '/gpfswork/rech/nhx/commun/scorep-7/bin/scorep-mpicxx',
        # 'ftn': '/gpfswork/rech/nhx/commun/scorep-7/bin/scorep-mpif90',
    }, # <end Jean-Zay system software stack>

    {
        'name': 'imaging-iotest-mkl',
        'target_systems': [
            'jeanzay',
            # For some reason ReFrame does not match these with the declarations in jeanzay.py
            # so revert to simple jeanzay            
            # 'jeanzay:jeanzay-gcc8-ompi4-opa-smod-cpu-short',
            # 'jeanzay:jeanzay-gcc8-ompi4-opa-smod-cpu-long',
        ],
        'modules': [
            'git-lfs/3.3.0',
            'intel-mpi',
            'hdf5/1.12.0',
            'fftw/3.3.10',
            'cmake/3.21.3',
        ],
         'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',

    },  # <end Jean-Zay system software stack with IMKL>


{
    'name': 'imaging-iotest-mkl',
    'target_systems': [
        'cscs-daint:daint-icc21-impi21-ib-umod-gpu',
        # <end - cscs partitions>                                                                                                                                                                                                                         
    ],
    'variables': [
        ['MPICH_MAX_THREAD_SAFETY', 'multiple'],
    ],
        'modules': [
            'PrgEnv-intel',
            'cray-hdf5',                                                                                                                                                                                                                                     
            'cray-fftw',
        ],
    },  # <end iotest environ with IMKL>                                                                                                                                                                                                                      
    {
        'name': 'imaging-iotest',
        'target_systems': [
            'cscs-daint:daint-gcc9-ompi4-ib-umod-gpu',
            # <end - cscs partitions>               
        ],
        'variables': [
            ['MPICH_MAX_THREAD_SAFETY', 'multiple'],
        ],
        'modules': [
            'PrgEnv-gnu',
            'cray-hdf5',
            'cray-fftw',
        ],
    },



]
