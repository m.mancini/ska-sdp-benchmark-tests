""""This file contains the environment config for RASCIL benchmark"""


rascil_environ = [
    {
        'name': 'rascil',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
            'boost/1.77.0',
        ],
        'variables': [
            ['SSH_WRAPPER', 'ssh'],
            ['NET_INTERFACE', 'ib0'],
            ['SCHEDULER_PORT', '8788'],
            ['DASHBOARD_PORT', '8789'],
        ],
        'target_systems': [
            'alaska:compute-gcc9-ompi4-roce-umod',
        ],
    },  # <end alaska>
    {
        'name': 'rascil',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
            'Boost/1.74.0-nompi',
        ],
        'variables': [
            # ssh does not work on JUWELS due to lack of keys
            ['SSH_WRAPPER', '\"srun --nodes 1 --ntasks 1 -w\"'],
            ['NET_INTERFACE', 'ib0'],
            ['SCHEDULER_PORT', '8788'],
            ['DASHBOARD_PORT', '8789'],
        ],
        'target_systems': [
            'juwels:login',
            'juwels-cluster:batch-gcc9-ompi4-ib-smod',
            'juwels-cluster:batch-gcc9-ompi4-ib-smod-mem192',
        ],
    },  # <end juwels>
    {
        'name': 'rascil',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
            'boost/1.77.0',
        ],
        'variables': [
            ['SSH_WRAPPER', 'oarsh'],
            ['NET_INTERFACE', 'ib0'],
            ['SCHEDULER_PORT', '8786'],
            ['DASHBOARD_PORT', '8787'],
        ],
        'target_systems': [
            'grenoble-g5k:dahu-gcc9-ompi4-opa-umod',
            'grenoble-g5k:troll-gcc9-ompi4-opa-umod',
        ],
    },  # <end grenoble>
    {
        'name': 'rascil',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
            'boost/1.77.0',
        ],
        'variables': [
            ['SSH_WRAPPER', 'oarsh'],
            ['NET_INTERFACE', 'br0'],
            ['SCHEDULER_PORT', '8786'],
            ['DASHBOARD_PORT', '8787'],
        ],
        'target_systems': [
            'nancy-g5k:gros-gcc9-ompi4-eth-umod',
        ],
    },  # <end nancy>
    {
        'name': 'rascil',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
            'boost/1.77.0',
        ],
        'variables': [
            ['SSH_WRAPPER', '\"srun --nodes 1 --ntasks 1 -w\"'],
            ['NET_INTERFACE', 'ib0'],
            ['SCHEDULER_PORT', '8788'],
            ['DASHBOARD_PORT', '8789'],
        ],
        'target_systems': [
            'jacamar-hpc-ci:grvingt-gcc9-ompi4-opa-umod',
        ],
    },  # <end jacamar HPC CI>
    {
        'name': 'rascil',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
            'boost/1.77.0',
        ],
        # 'variables': [
        #     ['SSH_WRAPPER', 'ssh'],
        #     ['NET_INTERFACE', 'ib0'],
        #     ['SCHEDULER_PORT', '8786'],
        #     ['DASHBOARD_PORT', '8787'],
        # ],
        'target_systems': [
            'cscs-daint:daint-gcc9-ompi4-ib-umod-gpu',
        ],
    },  # <end cscs daint>

    {
        'name': 'rascil',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
            'Boost/1.77.0-CrayGNU-21.09-python3',
        ],
        'variables': [
            ['SSH_WRAPPER', 'ssh'],
            ['NET_INTERFACE', 'ib0'],
            ['SCHEDULER_PORT', '8786'],
            ['DASHBOARD_PORT', '8787'],
        ],
        'target_systems': [
            'cscs-daint:daint-gcc9-ompi4-ib-smod-gpu',
        ],
    },  # <end cscs daint>

]
