""""This file contains the environment config for HPL benchmark"""


hpl_environ = [
    {
        'name': 'intel-hpl',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'modules': [
            'intel-oneapi-mkl/2021.3.0',
        ],
        'variables':[
            ['XHPL_BIN', '$MKLROOT/benchmarks/mp_linpack/xhpl_intel64_dynamic'],
        ],
        'target_systems': [
            'alaska:compute-icc21-impi21-roce-umod',
            # <end - alaska partitions>
            'grenoble-g5k:dahu-icc21-impi21-opa-umod',
            # <end - grenoble-g5k partitions>
            'juwels-cluster:batch-icc21-impi21-ib-umod',
            # <end juwels partitions>
            'nancy-g5k:gros-icc21-impi21-eth-umod',
            # <end - nancy-g5k partitions>
        ],
    },
    {
        'name': 'gnu-hpl',
        'cc': '',
        'cxx': '',
        'ftn': '',
        'modules': [
            'amdblis/3.0',
        ],
        # 'variables': [
        #     ['UCX_TLS', 'ud,rc,dc,self']
        # ],
        'target_systems': [
            'juwels-booster:booster-gcc9-ompi4-ib-umod',
            # <end juwels partitions>
        ],
    },
    {
        'name': 'intel-hpl',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'modules': [
            'daint-mc',
            'intel/2021.3.0',
        ],
        'variables':[
            ['XHPL_BIN', '$MKLROOT/benchmarks/mp_linpack/xhpl_intel64_dynamic'],
            #intel/2021.3.0 module does not set all the necessary LD_LIBRARY_PATH  required for xhpl_intel64_dynamic
            ['LD_LIBRARY_PATH', '/opt/intel/oneapi/mpi/2021.3.0/lib:/opt/intel/oneapi/mpi/2021.3.0/lib/release'],
        ],
        'target_systems': [
            'cscs-daint:daint-icc21-impi21-ib-umod-gpu',
            # <end - cscs partitions>
        ],
    },

    {
        'name': 'gnu-hpl',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'modules': [        ],
        'target_systems': [
            'licallo',
            # <end licallo partitions>
        ],
    },


{
    'name': 'intel-hpl',
    'cc': 'mpicc',
    'cxx': 'mpicxx',
    'ftn': 'mpif90',
    'modules': [
        'intel-oneapi/2023.1.0',
        'intel-oneapi-mpi/2021.9.0',
    ],
    'variables':[
        ['XHPL_BIN', '$MKLROOT/benchmarks/mp_linpack/xhpl_intel64_dynamic'],
    ],
    'target_systems': [
        'licallo',
        # <end - licallo partitions>
        ],
},

    
    {
        'name': 'intel-hpl',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'modules': [
            'intel-oneapi-mkl/2023.1',
            'intel-mpi',
        ],
        'variables':[
            ['XHPL_BIN', '$MKLROOT/benchmarks/mp_linpack/xhpl_intel64_dynamic'],
        ],
        'target_systems': [
            'jeanzay',
            # <end - jeanzay partitions>
        ],
    },

    

]
