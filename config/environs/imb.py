""""This file contains the environment config for Intel MPI benchmarks"""


imb_environ = [
    {
        'name': 'imb',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'modules': [
            'intel-mpi-benchmarks/2019.6-openmpi-4.1.1',
        ],
        'target_systems': [
            'alaska:compute-gcc9-ompi4-roce-umod',
            # <end - alaska partitions>
            'grenoble-g5k:dahu-gcc9-ompi4-opa-umod',
            # <end - grenoble partitions>
            'juwels-cluster:batch-gcc9-ompi4-ib-umod',
            # <end - juwels partitions>
            'nancy-g5k:gros-gcc9-ompi4-eth-umod',
            # <end - nancy partitions>
        ],
    },
    {
        'name': 'imb',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'modules': [
            'intel/2021.3.0',
            'PrgEnv-intel',
            'cray-mpich-abi',
        ],
        'variables':[
            #cray-mpich-abi/7.7.18 module does not set all the necessary LD_LIBRARY_PATH  required for IMB-MPI1
            ['LD_LIBRARY_PATH','$CRAY_LD_LIBRARY_PATH:$LD_LIBRARY_PATH'],
            ['IMB_MPI1_PATH', '/opt/intel/oneapi/mpi/latest'],
            ['IMB_MPI1_BIN', '$IMB_MPI1_PATH/bin/IMB-MPI1'],
        ],
        'target_systems': [
            'cscs-daint:daint-icc21-impi21-ib-umod-gpu'
            # <end - cscs daint partitions>
        ],
    },

    {
        'name': 'imb',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'modules': [
            'intel-oneapi/2023.1.0',
            'intel-oneapi-mpi/2021.9.0',
        ],
        'target_systems': [
            'licallo',
            # <end - licallo partitions>
        ],
    },

        {
        'name': 'imb',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'modules': [
            'intel-mpi/2021.9',
        ],
        'target_systems': [
            'jeanzay',
            # <end - jeanzay partitions>
        ],
    },


]
