""""This file contains the environment config for IDG benchmark"""


idg_test_environ = [
    {
        'name': 'idg-test',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'modules': [
            'cuda/11.2.0',
            'fftw/3.3.9', 'cmake/3.21.1',
        ],
        'target_systems': [
            'nancy-g5k:grouille-gcc9-ompi4-eth-umod-nvgpu',
            # <end - nancy partitions>
        ],
    },
    {
        'name': 'idg-test',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
            'cudatoolkit',
            'fftw/3.3.10', 'cmake/3.22.1',
        ],
        'target_systems': [
            'cscs-daint:daint-gcc9-ompi4-ib-umod-gpu',
            # <end - cscs partitions>
        ],
    },
    {
        'name': 'idg-test',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'variables': [
            ['LD_LIBRARY_PATH', 
             '/usr/local/cuda-11.3/lib64:/usr/local/cuda-11.3/compat:/usr/local/cuda/lib64:$LD_LIBRARY_PATH'],
            ['CMAKE_PREFIX_PATH', 
             '"/usr/local/cuda/compat:/usr/local/cuda:/usr/local/cuda-11.3"'],
            ['CXXFLAGS', '"-isystem /usr/local/cuda/include"'],
        ],
        'target_systems': [
            'gitlab-ci:k8-runner',
            # <end - Gitlab CI runner>
        ],
    },
]
