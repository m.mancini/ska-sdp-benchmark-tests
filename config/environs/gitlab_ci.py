""""This file contains the environment config for GitLab CI"""


ci_environ = [
    {
        'name': 'ci-environ',
        'cc': 'mpicc',
        'cxx': 'mpicxx',
        'ftn': 'mpif90',
        'target_systems': [
            'gitlab-ci',
        ]
    },
]
