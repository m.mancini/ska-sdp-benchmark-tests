"""ReFrame configuration file."""

import os
import sys

# Add root dir to PYTHONPATH
ROOT_DIR = os.path.dirname(os.path.realpath(__file__))
try:
    sys.path.index(ROOT_DIR)
except ValueError:
    sys.path.append(ROOT_DIR)

# Import each system/partition config files
from config.systems.alaska import alaska_system_config
from config.systems.grenoble_g5k import grenoble_g5k_system_config
from config.systems.juwels_booster import juwels_booster_system_config
from config.systems.juwels_cluster import juwels_cluster_system_config
from config.systems.lyon_g5k import lyon_g5k_system_config
from config.systems.marconi100 import marconi100_system_config
from config.systems.nancy_g5k import nancy_g5k_system_config
from config.systems.ska_k8_ci import ci_runner_config
from config.systems.hpc_ci import hpc_ci_config
from config.systems.cscs import cscs_system_config
from config.systems.jeanzay import jeanzay_system_config
from config.systems.g5k_lyon import g5k_lyon_system_config
from config.systems.licallo import licallo_system_config

# Import environments for each benchmark
from config.environs.babel_stream import babel_stream_environ
from config.environs.cng_test import cng_test_environ
from config.environs.gitlab_ci import ci_environ
from config.environs.gpu_rdma_test import gpu_rdma_test_environ
from config.environs.hpcg import hpcg_environ
from config.environs.hpl import hpl_environ
from config.environs.idg_test import idg_test_environ
from config.environs.ior import ior_environ
from config.environs.imaging_iotest import imaging_iotest_environ
from config.environs.imb import imb_environ
from config.environs.nccl_test import nccl_test_environ
from config.environs.numpy import numpy_environ
from config.environs.numpy_cuda import numpy_cuda_environ
from config.environs.rascil import rascil_environ
from config.environs.rapthor import rapthor_environ
from config.environs.stream import stream_environ
from config.environs.funclib import funclib_environ

site_configuration = {
    'systems': [
        {
            'name': 'catalina',
            'descr': 'My personal iMac',
            'hostnames': ['mahendra'],
            'modules_system': 'nomod',
            'partitions': [
                {
                    'name': 'default',
                    'scheduler': 'local',
                    'launcher': 'local',
                    'environs': ['gnu', 'clang'],
                }
            ]
        },  # end catalina
        {
            **alaska_system_config,
        },  # end alaska
        {
            **ci_runner_config,
        },  # end gitlab k8 ci runner
        {
            **grenoble_g5k_system_config,
        },  # end grenoble-g5k
        {
            **hpc_ci_config,
        },  # end CI runner config
        {
            **juwels_booster_system_config,
        },  # end juwels booster
        {
            **juwels_cluster_system_config,
        },  # end juwels cluster
        {
            **lyon_g5k_system_config,
        },  # end lyon-g5k
        {
            **marconi100_system_config,
        },  # end Marconi100
        {
            **nancy_g5k_system_config,
        },  # end nancy-g5k
        {
            **cscs_system_config,
        },  # end cscs
        {
            **jeanzay_system_config,
        },  # end Jean-Zay
        {
            **g5k_lyon_system_config,
            # end G5K clusters
        },
        {
            **licallo_system_config,
            # end G5K clusters
        },

        # < insert new systems here >
        {
            'name': 'generic',
            'descr': 'Generic system. When no system is matched, it falls back to generic system',
            'hostnames': ['.*'],
            'partitions': [
                {
                    'name': 'default',
                    'scheduler': 'local',
                    'launcher': 'local',
                    'environs': ['builtin', 'gnu', 'testing'],
                    'max_jobs': 1,
                    'extras': {
                        'internet': False
                    },
                },
            ],
        },  # end generic
    ],
    'environments': [
        {
            'name': 'gnu',
            'cc': 'gcc',
            'cxx': 'g++',
            'ftn': 'gfortran'
        },
        {
            'name': 'clang',
            'cc': 'clang',
            'cxx': 'clang++',
            'ftn': ''
        },
        *babel_stream_environ,
        *ci_environ,
        *cng_test_environ,
        *gpu_rdma_test_environ,
        *imaging_iotest_environ,
        *imb_environ,
        *hpcg_environ,
        *hpl_environ,
        *idg_test_environ,
        *ior_environ,
        *nccl_test_environ,
        *numpy_environ,
        *numpy_cuda_environ,
	*rapthor_environ,
        *rascil_environ,
        *stream_environ,
        *funclib_environ,
        {
            'name': 'builtin',
            'cc': 'gcc',
            'cxx': 'g++',
            'ftn': 'gfortran',
            'target_systems': [
                'alaska', 'grenoble-g5k',
                'juwels-cluster', 'juwels-booster',
                'lyon-g5k', 'marconi100',
                'nancy-g5k', 'generic', 'jacamar-hpc-ci',
                'cscs-daint', 'jeanzay', 'g5k-lyon',
                'licallo'
            ]
        },
        {
            'name': 'testing',
            'cc': 'gcc',
            'cxx': 'g++',
            'ftn': 'gfortran',
            'target_systems': ['generic']
        },
    ],
    'logging': [
        {
            'level': 'debug',
            'handlers': [
                {
                    'type': 'file',
                    'name': 'reframe.log',
                    'level': 'debug',
                    'format': '[%(asctime)s] %(levelname)s: %(check_name)s: %(message)s',  # noqa: E501
                    'append': False
                },
                {
                    'type': 'stream',
                    'name': 'stdout',
                    'level': 'info',
                    'format': '%(message)s'
                },
                {
                    'type': 'file',
                    'name': 'reframe.out',
                    'level': 'info',
                    'format': '%(message)s',
                    'append': False
                }
            ],
            'handlers_perflog': [
                {
                    'type': 'filelog',
                    # make this the same as output filenames which are ('sysname', 'partition',
                    # 'environ', 'testname', 'filename')
                    'prefix': '%(check_system)s/%(check_partition)s/%(check_environ)s'
                              '/%(check_name)s',  # <testname>.log gets appended
                    'level': 'info',
                    # added units here - see Reference:
                    # https://reframe-hpc.readthedocs.io/en/latest/config_reference.html?highlight=perflog#logging-.handlers_perflog
                    'format': '%(check_job_completion_time)s|reframe %(version)s|%(check_info)s|'
                              'jobid=%(check_jobid)s|%(check_perf_var)s=%(check_perf_value)s|'
                              '%(check_perf_unit)s|ref=%(check_perf_ref)s '
                              '(l=%(check_perf_lower_thres)s, '
                              'u=%(check_perf_upper_thres)s)|%(check_tags)s',  # noqa: E501
                    'datefmt': '%FT%T%:z',
                    'append': True
                }
            ]
        }
    ],
    'general': [
        {
            'check_search_path': ['./apps'],
            'check_search_recursive': True,
            'purge_environment': True,
            'remote_detect': False,
            'git_timeout': 30,
            # We use Spack envs to update module path for each partition. We disable ReFrame
            # checking the module files as they are not on path at the time of checking
            #'resolve_module_conflicts': False,

            # in case of test development:
            # 'keep_stage_files': True,
            # 'clean_stagedir': False,
        }
    ]
}
