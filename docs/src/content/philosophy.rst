Framework philosophy
===================================

Adding new tests
----------------------

To add a new test to the benchmark suite, follow the following steps:
1. Define whether the test belongs into level 0, level 1 or level 2. Then create a folder in the corresponding location and add the following files:

- ``reframe_<test_name>.py``: This is the main test file where we define the test class derived from the Benchmark base classes :code:`BenchmarkBase`, :code:`RunOnlyBenchmarkBase` or :code:`CompileOnlyBenchmarkBase`. Make sure to use the one corresponding to your specific needs. Those classes are developed according to the ReFrame ideas documented in https://reframe-hpc.readthedocs.io/en/stable/tutorial_advanced.html#writing-a-run-only-regression-test.
- ``TEST_NAME.ipynb``: Jupyter notebook to plot the performance metrics derived from the test
- ``README.md``: A simple readme file that gives high level instructions on where to find the documentation of the test.

2. Define the test procedure:

    - Does the test need some sources or packages from the internet, be it its own sources, python packages or any other dependencies? If yes, create a test dependency that fetches everything. Every test or test dependency that accesses the internet needs to inherit from the ``UsesInternet`` mixin.

    .. literalinclude:: ../../../apps/level1/idg_test/reframe_idgtest.py
       :lines: 88-93

    - Does the test need to compile fetched dependencies? If yes, create a test dependency that builds the sources. If the sources are fetched in a previous test, be sure to include this as a dependent fixture: ``app_src = fixture(DownloadTest, scope='session')``.

    .. literalinclude:: ../../../apps/level1/idg_test/reframe_idgtest.py
       :lines: 96-152

3. Write the test itself.

    - Define all dependencies as fixture, all parameters as `parameter` and all variables as `variable`. Tests are run for all permutations of parameters, whereas variables can define specific behaviour for a single run (like number of nodes).
    - Set the `valid_prog_environs` and the `valid_systems` in the `__init__` method.
    - Define the executable and executable options.
    - Define the Sanity Patterns. You can define which patterns must and must not appear in the stdout and stderr.

    .. literalinclude:: ../../../apps/level1/imaging_iotest/reframe_iotest.py
       :lines: 500-516

    - Define the Performance Functions. To extract data from the output stream it is necessary to extract them using regular expressions.

    .. literalinclude:: ../../../apps/level1/idg_test/reframe_idgtest.py
       :lines: 287-365

The sanity- and performance functions are both based on the concept of "Deferrable Functions". Be sure to check out the `official documentation <https://reframe-hpc.readthedocs.io/en/v3.12.0/deferrable_functions_reference.html>`__ on how to use them properly.

Those steps allow you to write a basic ReFrame test. For more in-detail view, take a look at the `ReFrame documentation <https://reframe-hpc.readthedocs.io/en/v3.12.0/>`__.
There is no strict convention on how to name the test. Already provided tests can be used as templates to write new tests.
The idea is to provide an environment for a given test and define all the test related variables like modules to load, environment variables to define within this environment. Also, we need to add the ``target_systems`` to this environments on the systems that we would like to run these tests. The details of adding a new environment and system are presented below.

4. Write a unit test procedure. You can take for example the test class for ``CondaEnvManager`` located in ``unittests/test_conda_env_manager.py``.

   - Create a file test with a class test for your benchmark :

     .. literalinclude:: ../../../unittests/test_conda_env_manager.py
       :lines: 1-11
	  
   - Add a test method for each functionnality :

     .. literalinclude:: ../../../unittests/test_conda_env_manager.py
       :lines: 13-21

   - Check results via the ``self.assertTrue`` and ``self.assertFalse`` methods (`unittest doc <https://docs.python.org/3/library/unittest.html>`) :

     .. literalinclude:: ../../../unittests/test_conda_env_manager.py
       :lines: 19-20

   - Add the name of your unit test file (without the extension) in the list of unit tests to check in the ``sdp-unittest.sh`` script

     .. literalinclude:: ../../../unittests/test_conda_env_manager.py
       :lines: 19-20


	       
We recommand to use systematically the ``logging`` module (`logging doc <https://docs.python.org/3/library/logging.html>`) which provides 6 custom level of screen outputs (e.g. ``CRITICAL``, ``ERROR``, ``WARNING``, ``INFO``, ``DEBUG``, ``NOSET``). 

To perform our unit test procedure, we provide an experimental bash script ``sdp-unittest.sh`` to launch our different unit tests automatically with several available options. We use the package ``pytest`` (`pytest doc <https://docs.pytest.org/en/7.1.x/contents.html>`) to purpose a parallelized and efficient procedure for the unit test step. 

Adding new system
----------------------------------------------

Every time we want to add a new system, typically we will need to follow these steps:

- Create a new python file ``<system_name>.py`` in ``config/systems`` folder.
- Add system configuration and define partitions for the system. More details on how to define a partition and naming conventions are presented later.
- Import this file into ``reframe_config.py`` and add this new system in the ``site_configuration``.
- The final step would be get the processor info using ``--detect-host-topology`` option on ReFrame of system nodes, place in the ``toplogies`` folder and include the file in ``processor`` key for each partition.

The user is advised to consult the `ReFrame documentation <https://reframe-hpc.readthedocs.io/en/stable/configure.html>`_ before doing so. The provided systems can be used as a template to add new systems.

We try to follow a certain convention in defining the system partition. Firstly, we define partitions, either physical or abstract, based on compiler toolchain and MPI implementation such that when we use this system, modules related to compiler and MPI will be loaded. Rest of the modules that are related to test will be added to the ``environs`` which will be discussed later. Consequently, we should also name these partitions in such a way that we can have a standard scheme. The benefit of having such a scheme is two-fold: able to get high level overview of partition quickly and by choosing an appropriate names, we can filter the systems for the tests easily. An example use case is that we want to run a certain test on all partitions that support GPUs. Using a partition name with ``gpu`` as suffix, we can simply filter all the partitions looking for a match with string ``gpu``.

We use the convention ``{prefix}-{compiler-name-major-ver}-{mpi-name-major-ver}-{interconnect-type}-{software-type}-{suffix}``.

- ``Prefix`` can be name of the partition or cluster.
- ``compiler-name-major-ver`` can be as follows:
   - ``gcc9``: GNU compiler toolchain with major version 9
   - ``icc20``: Intel compiler toolchain with major version 2020
   - ``xl16``: IBM XL toolchain with major version 16
   - ``aocc3``: AMD AOCC toolchain with major version 3
- ``mpi-name-major-ver`` is the name of the MPI implementation. Some of them are:
   - ``ompi4``: OpenMPI with major version 4
   - ``impi19``: Intel MPI with major version 2019
   - ``pmpi5``: IBM Spectrum MPI with major version 5
   - ``smpi10``: IBM Spectrum MPI with major version 10
- ``interconnect-type`` is type of interconnect on the partition.
   - ``ib``: Infiniband
   - ``rocm``: RoCE
   - ``opa``: Intel Omnipath
   - ``eth``: Ethernet TCP
- ``software-type`` is type of software stack used.
   - ``smod``: System provided software stack
   - ``umod``: User built software stack using Spack
- ``suffix`` can indicate any special properties of the partitions like gpu, high memory nodes, high priority job queues, *etc*. There can be multiple suffices each separated by a hyphen.

.. important::

  If the package uses calendar versioning, we use only last two digits of the year in the name to be concise. For example, Intel MPI 2019.* would be ``impi19``.

For instance, in the configuration shown in :ref:`content/reframe:ReFrame configuration` ``compute-gcc9-ompi4-roce-umod`` tells us that the partition has GCC compiler with OpenMPI. It uses RoCE as interconnect and the softwares are built in user space using Spack.

.. important::

  It is recommended to stick to this convention and there can be more possibilities for each category which should be added as we add new systems.


Adding new environment
----------------------------------------------

Adding a new system is not enough to run the tests on this system. We need to tell our ReFrame tests that there is a new system available in the config. In order to minimise the redundancy in adding configuration details and avoid modifying the source code of the test, we choose to provide a ``environ`` for each test. For example, there is HPL test in ``apps/level0/hpl`` folder and for this test we define a environ in ``config/environs/hpl.py``.

.. note::

  System partitions and environments should have one-to-one mapping. It means, whatever environment we define within ``environs`` section in the system partition, we should put that partition within ``target_systems`` in each ``environ``.


All the modules that are needed to run the test, albeit compiler and MPI, will be added to the ``modules`` section in each ``environ``. For example, lets take a look at ``hpl.py`` file

.. literalinclude:: ../../../config/environs/hpl.py


There are two different environments namely ``intel-hpl`` and ``gnu-hpl``. As names suggests, ``intel-hpl`` uses HPL benchmark shipped out of MKL optimized for Intel chips. Whereas we use ``gnu-hpl`` for other chips like AMD using GNU toolchain. Notice that ``target_systems`` for ``intel-hpl`` has only partitions that have Intel MPI implementation (``impi`` in the name) whereas the ``gnu-hpl`` has ``target_systems`` have OpenMPI implementation. Within the test, we define only the ``valid program`` and find valid systems by filtering all systems that have the given environment defined for them.

For instance, we defined a new system partition that has Intel chip with name as ``mycluster-gcc-impi-ib-umod``. If we want HPL test to run on this system partition, we add ``intel-hpl`` to ``environs`` section in system partition and similarly add the name of this partition to ``target_systems`` in ``intel-hpl`` environment. Once we do that, the test will run on this partition without having to modify anything in the source code of the test.

If we want to add a new test, we will need to add new environment and following steps should be followed:

- Create a new file ``<env_name>.py`` in ``config/environs`` folder and add environment configuration for the tests. **It is important** that we add this new environment to existing and/or new system partitions that are defined in ``target_systems`` of the environment configuration.
- Finally, import ``<env_name>.py`` in the main ReFrame configuration file ``reframe_config.py`` and add it to the configuration dictionary.


Adding Spack configuration test
----------------------------------

After we define a new system, we need software stack on this system to be able to run tests on it. If the user chooses to use platform provided software stack, this step can be skipped. We need to define Spack config files in order to deploy the software stack. We can user existing config files provided for different systems as a base. Typically, we should only change ``compilers.yml``, ``modules.yml`` and ``spack.yml`` files for new system. We need to update the system compiler version and their paths in ``compilers.yml`` and also in ``modules.yml`` file in ``core_compilers`` section. Similarly, the desired software stack that will be installed on the system is defined in ``spack.yml`` file.

Once these configuration files are ready, we need to create a new folder in ``spack/spack_tests`` folder with name of the system and place all configuration files in ``configs/`` and define a ReFrame test to deploy this software stack. The user can use the existing test files as template. The ReFrame test file *per se* is very minimal and user needs to put the name of the cluster and path where Spack must be installed in the test body.

Adding Conda environment via CondaEnvManager
----------------------------------

CondaEnvManager is a class to provide a specific conda environment for each test (benchmark) we want to create. The advantage is we remove the different problem of dependancies we could have with the ska-sdp-benchmark environment (e.g. a different version of numpy package). For the moment, we provide this approach only for the ``level0/cpu/fft`` test and for the associated unit test. We will use the unit test example as a basis below.

*Note : The* ``self.assertTrue()`` and ``self.assertFalse()`` *are just unit test methods.*

Enable the module
^^^^^^^^^^^^^^^^^^
    .. literalinclude:: ../../../unittests/test_conda_env_manager.py
       :lines: 2

Create a conda environment
^^^^^^^^^^^^^^^^^^
When you instance your class, you don't create directly a conda environement with the name you provide :
    .. literalinclude:: ../../../unittests/test_conda_env_manager.py
       :lines: 25-26

Remove a conda environment
^^^^^^^^^^^^^^^^^^
Remove the created environment and all packages installed.
    .. literalinclude:: ../../../unittests/test_conda_env_manager.py
       :lines: 18

Install a package via pip
^^^^^^^^^^^^^^^^^^
The ``install_pip`` method provide a generic method which is able to download and install directly a package or install from a local folder a package if the package already exists (yet downloaded). You can also provide a ``requirement.txt`` file directory to install directly multiple packages (e.g. *Download and install manually a list of packages via pip*).
    .. literalinclude:: ../../../unittests/test_conda_env_manager.py
       :lines: 58-60

Download locally a package via pip
^^^^^^^^^^^^^^^^^^
Download locally in a cache folder packages by environment. It is usefull if you have not a connection on a calculation cluster :
    .. literalinclude:: ../../../unittests/test_conda_env_manager.py
       :lines: 66-68

	       
Download and install locally a list of packages via pip
^^^^^^^^^^^^^^^^^^
We can download via a ``requirement.txt`` file a list a packages for an environment. Need to provide the directory of your requirement file for the download and installations steps : 
    .. literalinclude:: ../../../unittests/test_conda_env_manager.py
       :lines: 78-86

Purge the pip cache
^^^^^^^^^^^^^^^^^^
Remove packages in the cache folder for a created environment. 
    .. literalinclude:: ../../../unittests/test_conda_env_manager.py
       :lines: 90


Separation of Parts that Access the Internet
----------------------------------
Any test or test dependency that accesses the internet needs to implement the mixin ``UsesInternet``. This mixin ensures that parts that need to
download assets from the internet are run on a partition where internet can be accessed. Any test that accesses the internet and does not
implement the ``UsesInternet`` mixin is **not guaranteed** to have internet available and might fail.

Best practice is to separate all dependencies that need to access the web into dependencies, be it dataset downloads, sources downloads or package
installations that fetch from repositories. Those dependencies must implement the ``UsesInternet`` mixin and should be defined as fixtures for the test.
