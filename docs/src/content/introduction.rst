Getting Started
===================================

Context
---------

This repository contains the SDP benchmark tests for various levels of benchmarking. `ReFrame <https://reframe-hpc.readthedocs.io/en/stable/index.html>`_ is used as a framework to package these benchmarks to perform automated tests on various HPC systems. It is designed to easily compare results from different systems, system configurations and/or environments. There are three levels of benchmarks defined in the package namely,

- **Level 0**: Kernel benchmarks that characterise various components of the system like FLOPS performance, memory bandwidth, network performance, *etc*.
- **Level 1**: These are representative pieces of real workflows that can be used to characterise the workload of the pipelines. These include processing functions of radio astronomy pipelines like FFTs, gridding, prototype codes, *etc*.
- **Level 2**: These will be the entire pipelines or workflows of the radio-astronomy softwares like WSClean, RASCIL, ASKAP, *etc*.

.. note::
  For level 1 and 2 benchmarks, runtime configurations should be chosen in such a way to model the intended computational workload. Whereas level 0 benchmarks can be used to characterise various existing HPC systems in terms of raw performance.

Some part of this work has been inspired from `hpc-tests <https://github.com/stackhpc/hpc-tests>`_ work from `StackHPC <https://www.stackhpc.com/>`_ who used ReFrame to build performance tests for HPC platforms.


Currently supported benchmarks
--------------------------------

Level 0
~~~~~~~~~~~~~~~~~~~~~~

- Sample ``numpy`` operations benchmark is included in the tests.
- `Babel Stream <http://uob-hpc.github.io/BabelStream/>`_
- `GPUDirect RDMA (GDR) test <https://developer.nvidia.com/gpudirect>`_.
- `HPCG <https://www.hpcg-benchmark.org/>`_
- `HPL <https://www.netlib.org/benchmark/hpl/>`_
- `IMB <https://www.intel.com/content/www/us/en/developer/articles/technical/intel-mpi-benchmarks.html>`_
- `IOR <https://ior.readthedocs.io/en/latest/>`_
- `NCCL tests <https://github.com/NVIDIA/nccl-tests>`_
- `STREAM <https://www.cs.virginia.edu/stream/>`_
- numpy / cupy FFTs
- `Part of HIPPO Team's Funclib <https://gitlab.com/ska-telescope/sdp/ska-sdp-func>`_

Level 1
~~~~~~~~~~~~~~~~~~~~~~

- `CUDA NIFTY gridder <https://gitlab.com/ska-telescope/sdp/ska-gridder-nifty-cuda>`_
- `Image Domain Gridder (IDG) Test <https://git.astron.nl/RD/idg>`_
- `Imaging IO Test <https://gitlab.com/ska-telescope/sdp/ska-sdp-exec-iotest>`_

Level 2
~~~~~~~~~~~~~~~~~~~~~~

- `RASCIL <https://gitlab.com/ska-telescope/external/rascil>`_

All the tests are defined in a portable fashion in ``reframe_<application>.py`` file in each application directory. Results are compared and plotted using Juypter notebooks, with a ``<application>.ipynb`` file in each application directory.

Software stack
--------------------------------

The benchmark tests can be run using either platform provided packages or it is possible to deploy our own software stack in the user space using `Spack <https://spack.readthedocs.io/>`_. Moreover, ReFrame gives us a framework to test both platform provided and user deployed software stacks using the notion of partitions. There is no recommendation when it comes to which software stack to use and it depends on the final objective. For example, if we want to compare two different types of architectures, it is advisable to deploy the same software stack on both platforms and then compare the results. More details on how to use Spack and build packages are presented in :ref:`content/spack:Installing packages`.
