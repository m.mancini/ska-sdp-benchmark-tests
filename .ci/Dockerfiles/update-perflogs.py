"""Update artifacts from cache artifacts in CI pipeline"""

import os
import sys
import re
from pathlib import Path

if len(sys.argv) < 3:
    print("Please provide two paths of perflogs [Optional argument is CI "
          "system and partition names]")
    exit(1)

new_perflog_dir = sys.argv[1]
old_perflog_dir = sys.argv[2]

# Initialise CI system and partition names
ci_sys_name = '.*'
ci_part_name = '.*'

# Get system, partition names
if len(sys.argv) == 4:
    ci_name = sys.argv[3]
    ci_name_split = ci_name.split(':')
    if len(ci_name_split) == 1:
        ci_sys_name = ci_name_split[0]
    elif len(ci_name_split) == 2:
        ci_sys_name, ci_part_name = ci_name_split

for root, dirs, files in os.walk(new_perflog_dir):
    for file in files:
        # Perflogs are only .log files
        if (re.search(ci_sys_name, root) and re.search(ci_part_name, root) and
                file.endswith('.log')):
            # Newly created content
            new_content = open(os.path.join(root, file), 'r').read()
            old_perflog_file_path = Path(*Path(root).parts[1:])
            try:
                # Try to read old perf log if exists
                old_content = open(os.path.join(old_perflog_file_path, file),
                                   'r').read()
            except FileNotFoundError:
                # If no file is found, create intermediate folders
                os.makedirs(old_perflog_file_path, exist_ok=False)
                old_content = new_content
            else:
                # Add new content to old
                old_content += new_content
            # Update file or create new file
            file_to_write = os.path.join(old_perflog_file_path, file)
            with open(file_to_write, 'w') as new_perf_file:
                new_perf_file.write(old_content)
