# pylint: disable=C0301
"""
IDG MICRO
-------------------------

Context
~~~~~~~~~~~~~~~~~~~~~~~~

The image-domain gridder (IDG) is a new, fast gridder that makes w-term correction and 
a-term correction computationally very cheap. It performs extremely well on gpus. 
The source code is hosted on ASTRON
`GitLab <https://gitlab.com/ska-telescope/sdp/ska-sdp-idg-bench>`_ repository and documentation
can be found `here <https://www.astron.nl/citt/IDG/quick-start.html>`__.

Test variables
~~~~~~~~~~~~~~~~~~~~~~~~

Environment variables
~~~~~~~~~~~~~~~~~~~~~~~~

By default, the test will create a ``conda`` environment and run inside it for the 
sake of isolation. This can be controlled using env variable ``CREATE_CONDA_ENV``. 
By setting it to ``NO``, the test WILL NOT create ``conda`` environment.

Similarly, the performance metrics are monitored using the `perfmon <https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon>`_ toolkit. If
the user does not want to monitor metrics, it can be achieved by setting 
``MONITOR_METRICS=NO``. 

.. _idg usage:

Usage
~~~~~~~~~~~~~~~~~~~~~~~~

The tests can be run using the following commands:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/gpu/idg_micro/reframe_idgtest.py --run --performance-report

If we want to change the variables to non default values, we should use ``-S`` flag. For example, if we want to run only 5 major cycles and 64 frequency channels, use

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/gpu/idg_micro/reframe_idgtest.py -S num_cycles=5 -S num_chans=64 --run --performance-report

Test class documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""
# pylint: enable=C0301

import os
from pathlib import Path

import reframe as rfm
import reframe.utility.sanity as sn  # pylint: disable=import-error
from modules.base_class import CompileOnlyBenchmarkBase
from reframe.core.builtins import fixture, run_before, run_after, performance_function, require_deps
from reframe.utility import udeps  # pylint: disable=import-error

from modules.reframe_extras import AppBase
from modules.reframe_extras import FetchSourcesBase
from modules.utils import filter_systems_by_env

# pylint: disable=C0116,E0401,E0602,E1101,W0201

# Global variables
# Define varibales that will be shared across tests here
# Each test will run in its own conda environment. We define name of that environment here
ENV_NAME = 'rfm_IdgMicro'


class IdgMicroDownload(FetchSourcesBase):
    """Fixture to fetch IDG source code"""

    descr = 'Fetch source code of IDG'
    sourcesdir = 'https://gitlab.com/ska-telescope/sdp/ska-sdp-idg-bench.git'
    cnd_env_name = ENV_NAME


@rfm.simple_test
class IdgMicroBuild(CompileOnlyBenchmarkBase):
    """IDG test compile test"""

    descr = 'Compile IDG test from sources'

    # Share resource from fixture
    idg_test_src = fixture(IdgMicroDownload, scope='session')

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = [
            'idg-test',
        ]
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        self.maintainers = [
            'Stefano Corda (stefano.corda@epfl.ch)'
        ]
        # Cross compilation is not possible on certain g5k clusters as well as on cscs daint (GPU). 
        # We force the job to be non-local so building will be on remote node
        if 'g5k' in self.current_system.name or 'cscs-daint' in self.current_system.name:
            self.build_locally = False

    @run_before('compile')
    def set_sourcedir(self):
        """Set source path based on dependencies"""
        self.sourcesdir = self.idg_test_src.stagedir

    @run_before('compile')
    def set_prebuild_cmds(self):
        """Make local lib dirs"""
        self.lib_dir = os.path.join(self.stagedir, 'local')
        self.prebuild_cmds = [
            f'mkdir -p {self.lib_dir}',
        ]
        if os.getenv('AMD', 'NO') == 'NO':
            if os.getenv('BUILD_HIP', 'NO') == 'YES':
                self.prebuild_cmds.insert(0, 'export CUDA_PATH=$CUDA_ROOT')
                self.prebuild_cmds.insert(0, 'export HIP_PLATFORM=nvidia')
                self.prebuild_cmds.insert(0, 'export HIP_COMPILER=nvcc')
                self.prebuild_cmds.insert(0, 'export HIP_RUNTIME=cuda')
                self.prebuild_cmds.insert(0, 'export HIPCC_COMPILE_FLAGS_APPEND="-std=c++11"')

    @run_before('compile')
    def set_build_system_attrs(self):
        """Set build directory and config options"""
        self.build_system = 'CMake'
        self.build_system.builddir = os.path.join(self.stagedir, 'build')

        run_config = [f'-DCMAKE_INSTALL_PREFIX={self.lib_dir}']
        if os.getenv('AMD', 'NO') == 'YES':
            run_config.append('-DGPU_BRAND=AMD')
            run_config.append('-DBUILD_HIP=ON')
            run_config.append('-DBUILD_CUDA=OFF')
        else:
            run_config.append('-DGPU_BRAND=NVIDIA')
            run_config.append('-DBUILD_CUDA=ON')
            if os.getenv('BUILD_HIP', 'NO') == 'YES':
                run_config.append('-DBUILD_HIP=ON')
            else:
                run_config.append('-DBUILD_HIP=OFF')
        if os.getenv('BUILD_LIBPOWERSENSOR', 'NO') == 'YES':
            run_config.append('-DBUILD_LIBPOWERSENSOR=ON')
        else:
            run_config.append('-DBUILD_LIBPOWERSENSOR=OFF')

        self.build_system.config_opts = run_config

        self.build_system.max_concurrency = 8

    @run_before('compile')
    def set_postbuild_cmds(self):
        """Install libs"""
        self.postbuild_cmds = [
            'make install',
        ]

    @run_before('sanity')
    def set_sanity_patterns(self):
        """Set sanity patterns"""
        self.sanity_patterns = sn.assert_not_found('error', self.stderr)


@rfm.simple_test
class IdgMicro_Gridder(AppBase):
    """Main class of IDG benchmark tests"""

    time_limit = "0d2h0m0s"
    descr = 'IDG benchmarks for GPU performance.'


    # # Variable to define antenna layout
    # layout = variable(str, value='SKA1_low')

    # # Variable to define number of cycles
    # num_cycles = variable(int, value=10)

    # # Variable to define number of stations
    # num_stations = variable(int, value=10)

    # # Variable to define gridsize
    # gridsize = variable(int, value=8192)

    # # Variable to define number of channels
    # num_chans = variable(int, value=16)

    # # Variable to define number of nodes
    # # We always test on 1 node
    # num_nodes = variable(int, value=1)

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = [
            'idg-test',
        ]
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        self.maintainers = [
            'Stefano Corda (stefano.corda@epfl.ch)'
        ]
        self.exclusive_access = True

    # @run_after('init')
    # def check_vars(self):
    #     """Check test variables"""
    #     self.skip_if(
    #         self.layout not in ['SKA1_low', 'SKA1_mid'], msg='Layout must be '
    #         'either SKA1_low or SKA1_mid'
    #     )

    @run_after('init')
    def set_dependencies(self):
        """Set dependencies of the test"""
        self.depends_on('IdgMicroBuild', udeps.by_part)

    @require_deps
    def set_executable(self, IdgMicroBuild):
        """Set executable path and executable"""
        self.executable_path = Path(
            os.path.join(IdgMicroBuild().stagedir, 'build', 'tests', ' ')
        ).parent.absolute()
        self.sourcesdir = os.path.join(IdgMicroBuild().stagedir)
        if os.getenv('BUILD_HIP', 'NO') == 'YES':
            self.executable = './hip-gridder_v6'
        else:
            self.executable = './cuda-gridder_v6'

    @run_after('init')
    def set_tags(self):
        """Add tags to the test"""

        if os.getenv('BUILD_HIP', 'NO') == 'YES':
            programming_model = "HIP"
        else:
            programming_model = "CUDA"
        if os.getenv('AMD', 'NO') == 'YES':
            gpu_brand = "AMD"
        else:
            gpu_brand = "NVIDIA"

        self.tags |= {
            # f'layout={self.layout}',
            # f'num_stations={self.num_stations}',
            # f'num_cycles={self.num_cycles}',
            # f'gridsize={self.gridsize}',
            # f'num_chans={self.num_chans}',
            # str(self.num_nodes),
            f'GPU_brand={gpu_brand}',
            f'Programming_Model={programming_model}',
        }


    # @run_after('setup')
    # def get_num_nodes(self):
    #     """Get number of nodes from total cores requested and number of cores per node"""
    #     self.num_cores = self.num_nodes * self.current_partition.processor.num_cpus
    #     self.tags |= {
    #         f'num_nodes={self.num_nodes}',
    #         f'num_cores={self.num_cores}',
    #     }

    # @run_after('setup')
    # def set_num_tasks_job(self):
    #     """This method sets tasks for the job. We use this to override the num_tasks set for
    #     reservation. Using this approach we can set num_tasks to job in a more generic way"""
    #     self.num_tasks = (self.current_partition.processor.num_cpus // 
    #                       self.current_partition.processor.num_cpus_per_core)
    #     self.tags |= {
    #         f'num_procs={self.num_tasks}'
    #     }

    # @run_after('setup')
    # def set_env_vars(self):
    #     """Set environment variables"""
    #     self.variables = {
    #         'LAYOUT_FILE': '.'.join([self.layout, 'txt']),
    #         'NR_CYCLES': str(self.num_cycles),
    #         'NR_STATIONS': str(self.num_stations),
    #         'GRIDSIZE': str(self.gridsize),
    #         'NR_CHANNELS': str(self.num_chans),
    #         'TOTAL_NR_TIMESTEPS': str(43200),
    #         'NR_TIMESTEPS': str(1800),
    #     }

    @run_after('setup')
    def add_launcher_options(self):
        """Set job launcher options"""
        if self.launcher_name == 'mpirun':
            # We are patching the launcher for mpirun/mpiexec to remove -np arg
            self.job.launcher.options = [
                '-np 1',
            ]
        elif self.launcher_name == 'mpiexec':
            # We are patching the launcher for mpirun/mpiexec to remove -np arg
            self.job.launcher.options = [
                '-n 1',
            ]
        elif self.launcher_name == 'srun':
            # Same goes with --nodes=<num_tasks_job> flag
            self.job.launcher.options = [
                '--ntasks=1',
            ]

    @run_before('run')
    def pre_launch(self):
        """Set prerun commands. It includes setting scratch directory and pre run commands from
        base class"""
        self.prerun_cmds += [
            f'cd {self.executable_path}',
        ]

    @run_before('sanity')
    def set_sanity_patterns(self):
        """Set sanity patterns. Example stdout:
            gridder_v6:  305.36 ms, 5826.49 GFLOP/s,   16.23 GB/s,  359.08 FLOP/byte,  164.32 MVis/s,   94.46 W,   61.68 GFLOP/s/W,    1.74 Mvis/J 
        """
        self.sanity_patterns = sn.all([
            sn.assert_found('gridder_v6', self.stdout),
            #sn.assert_found('Total runtime', self.stderr),
            #sn.assert_found('Total throughput', self.stderr),
        ])

    @performance_function('s')
    def extract_time(self, kind='gridding'):
        """Performance extraction function for time. Sample stdout:
            gridder_v6:  305.36 ms, 5826.49 GFLOP/s,   16.23 GB/s,  359.08 FLOP/byte,  164.32 MVis/s,   94.46 W,   61.68 GFLOP/s/W,    1.74 Mvis/J 
        """

        return sn.extractsingle(rf'^\s*{kind}:\s+(?P<ms_value>\S+) ms,\s+(?P<gflop_value>\S+) GFLOP/s,\s+(?P<gb_value>\S+) GB/s,\s+(?P<ai_value>\S+) FLOP/byte,\s+(?P<mvis_value>\S+) MVis/s,\s+(?P<watt_value>\S+) W,\s+(?P<gflop_w_value>\S+) GFLOP/s/W,\s+(?P<mvis_j_value>\S+) MVis/J', self.stdout, 'ms_value', float)

    @performance_function('GFLOP/s')
    def extract_gflops(self, kind='gridding'):
        """Performance extraction function for time. Sample stdout:
            gridder_v6:  305.36 ms, 5826.49 GFLOP/s,   16.23 GB/s,  359.08 FLOP/byte,  164.32 MVis/s,   94.46 W,   61.68 GFLOP/s/W,    1.74 Mvis/J 
        """

        return sn.extractsingle(rf'^\s*{kind}:\s+(?P<ms_value>\S+) ms,\s+(?P<gflop_value>\S+) GFLOP/s,\s+(?P<gb_value>\S+) GB/s,\s+(?P<ai_value>\S+) FLOP/byte,\s+(?P<mvis_value>\S+) MVis/s,\s+(?P<mvis_watt>\S+) W,\s+(?P<gflop_w_value>\S+) GFLOP/s/W,\s+(?P<mvis_j_value>\S+) MVis/J', self.stdout, 'gflop_value', float)

    @performance_function('GB/s')
    def extract_gbs(self, kind='gridding'):
        """Performance extraction function for time. Sample stdout:
            gridder_v6:  305.36 ms, 5826.49 GFLOP/s,   16.23 GB/s,  359.08 FLOP/byte,  164.32 MVis/s,   94.46 W,   61.68 GFLOP/s/W,    1.74 Mvis/J 
        """

        return sn.extractsingle(rf'^\s*{kind}:\s+(?P<ms_value>\S+) ms,\s+(?P<gflop_value>\S+) GFLOP/s,\s+(?P<gb_value>\S+) GB/s,\s+(?P<ai_value>\S+) FLOP/byte,\s+(?P<mvis_value>\S+) MVis/s,\s+(?P<mvis_watt>\S+) W,\s+(?P<gflop_w_value>\S+) GFLOP/s/W,\s+(?P<mvis_j_value>\S+) MVis/J', self.stdout, 'gb_value', float)

    @performance_function('Flop/Byte')
    def extract_ai(self, kind='gridding'):
        """Performance extraction function for time. Sample stdout:
            gridder_v6:  305.36 ms, 5826.49 GFLOP/s,   16.23 GB/s,  359.08 FLOP/byte,  164.32 MVis/s,   94.46 W,   61.68 GFLOP/s/W,    1.74 Mvis/J 
        """

        return sn.extractsingle(rf'^\s*{kind}:\s+(?P<ms_value>\S+) ms,\s+(?P<gflop_value>\S+) GFLOP/s,\s+(?P<gb_value>\S+) GB/s,\s+(?P<ai_value>\S+) FLOP/byte,\s+(?P<mvis_value>\S+) MVis/s,\s+(?P<mvis_watt>\S+) W,\s+(?P<gflop_w_value>\S+) GFLOP/s/W,\s+(?P<mvis_j_value>\S+) MVis/J', self.stdout, 'ai_value', float)

    @performance_function('MVis/s')
    def extract_vis_thpt(self, kind='gridding'):
        """Performance extraction function for time. Sample stdout:
           gridder_v6:  305.36 ms, 5826.49 GFLOP/s,   16.23 GB/s,  359.08 FLOP/byte,  164.32 MVis/s,   94.46 W,   61.68 GFLOP/s/W,    1.74 Mvis/J 
        """

        return sn.extractsingle(rf'^\s*{kind}:\s+(?P<ms_value>\S+) ms,\s+(?P<gflop_value>\S+) GFLOP/s,\s+(?P<gb_value>\S+) GB/s,\s+(?P<ai_value>\S+) FLOP/byte,\s+(?P<mvis_value>\S+) MVis/s,\s+(?P<mvis_watt>\S+) W,\s+(?P<gflop_w_value>\S+) GFLOP/s/W,\s+(?P<mvis_j_value>\S+) MVis/J', self.stdout, 'mvis_value', float)

    @performance_function('W')
    def extract_watt(self, kind='gridding'):
        """Performance extraction function for time. Sample stdout:
           gridder_v6:  305.36 ms, 5826.49 GFLOP/s,   16.23 GB/s,  359.08 FLOP/byte,  164.32 MVis/s,   94.46 W,   61.68 GFLOP/s/W,    1.74 Mvis/J 
        """

        return sn.extractsingle(rf'^\s*{kind}:\s+(?P<ms_value>\S+) ms,\s+(?P<gflop_value>\S+) GFLOP/s,\s+(?P<gb_value>\S+) GB/s,\s+(?P<ai_value>\S+) FLOP/byte,\s+(?P<mvis_value>\S+) MVis/s,\s+(?P<watt>\S+) W,\s+(?P<gflop_w_value>\S+) GFLOP/s/W,\s+(?P<mvis_j_value>\S+) MVis/J', self.stdout, 'watt', float)

    @performance_function('GFLOP/s/W')
    def extract_glop_w(self, kind='gridding'):
        """Performance extraction function for time. Sample stdout:
           gridder_v6:  305.36 ms, 5826.49 GFLOP/s,   16.23 GB/s,  359.08 FLOP/byte,  164.32 MVis/s,   94.46 W,   61.68 GFLOP/s/W,    1.74 Mvis/J 
        """

        return sn.extractsingle(rf'^\s*{kind}:\s+(?P<ms_value>\S+) ms,\s+(?P<gflop_value>\S+) GFLOP/s,\s+(?P<gb_value>\S+) GB/s,\s+(?P<ai_value>\S+) FLOP/byte,\s+(?P<mvis_value>\S+) MVis/s,\s+(?P<mvis_watt>\S+) W,\s+(?P<gflop_w_value>\S+) GFLOP/s/W,\s+(?P<mvis_j_value>\S+) MVis/J', self.stdout, 'gflop_w_value', float)

    @performance_function('MVis/J')
    def extract_vis_j(self, kind='gridding'):
        """Performance extraction function for time. Sample stdout:
           gridder_v6:  305.36 ms, 5826.49 GFLOP/s,   16.23 GB/s,  359.08 FLOP/byte,  164.32 MVis/s,   94.46 W,   61.68 GFLOP/s/W,    1.74 Mvis/J 
        """

        return sn.extractsingle(rf'^\s*{kind}:\s+(?P<ms_value>\S+) ms,\s+(?P<gflop_value>\S+) GFLOP/s,\s+(?P<gb_value>\S+) GB/s,\s+(?P<ai_value>\S+) FLOP/byte,\s+(?P<mvis_value>\S+) MVis/s,\s+(?P<mvis_watt>\S+) W,\s+(?P<gflop_w_value>\S+) GFLOP/s/W,\s+(?P<mvis_j_value>\S+) MVis/J', self.stdout, 'mvis_j_value', float)

    @run_before('performance')
    def set_perf_patterns(self):
        """Set performance variables"""
        self.perf_variables = {
            'ms': self.extract_time(kind='gridder_v6'),
            'GFLOP/s': self.extract_gflops(kind='gridder_v6'),
            'GB/s': self.extract_gbs(kind='gridder_v6'),
            'FLOP/Byte': self.extract_ai(kind='gridder_v6'),
            'MVis/s': self.extract_vis_thpt(kind='gridder_v6'),
            'W': self.extract_watt(kind='gridder_v6'),
            'GFLOP/s/W': self.extract_glop_w(kind='gridder_v6'),
            'MVis/J': self.extract_vis_j(kind='gridder_v6'),
        }

    # @run_before('performance')
    # def set_reference_values(self):
    #     """Set reference perf values"""
    #     self.reference = {
    #         '*': {
    #             '*': (None, None, None, 's'),
    #             '*': (None, None, None, 'Mvis/s'),
    #         }
    #     }


@rfm.simple_test
class IdgMicro_Degridder(AppBase):
    """Main class of IDG benchmark tests"""

    time_limit = "0d2h0m0s"
    descr = 'IDG benchmarks for GPU performance.'

    # # Variable to define antenna layout
    # layout = variable(str, value='SKA1_low')

    # # Variable to define number of cycles
    # num_cycles = variable(int, value=10)

    # # Variable to define number of stations
    # num_stations = variable(int, value=10)

    # # Variable to define gridsize
    # gridsize = variable(int, value=8192)

    # # Variable to define number of channels
    # num_chans = variable(int, value=16)

    # # Variable to define number of nodes
    # # We always test on 1 node
    # num_nodes = variable(int, value=1)

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = [
            'idg-test',
        ]
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        self.maintainers = [
            'Stefano Corda (stefano.corda@epfl.ch)'
        ]
        self.exclusive_access = True

    # @run_after('init')
    # def check_vars(self):
    #     """Check test variables"""
    #     self.skip_if(
    #         self.layout not in ['SKA1_low', 'SKA1_mid'], msg='Layout must be '
    #         'either SKA1_low or SKA1_mid'
    #     )

    @run_after('init')
    def set_dependencies(self):
        """Set dependencies of the test"""
        self.depends_on('IdgMicroBuild', udeps.by_part)

    @require_deps
    def set_executable(self, IdgMicroBuild):
        """Set executable path and executable"""
        self.executable_path = Path(
            os.path.join(IdgMicroBuild().stagedir, 'build', 'tests', ' ')
        ).parent.absolute()
        self.sourcesdir = os.path.join(IdgMicroBuild().stagedir)
        if os.getenv('BUILD_HIP', 'NO') == 'YES':
            self.executable = './hip-degridder_v6'
        else:
            self.executable = './cuda-degridder_v6'

    @run_after('init')
    def set_tags(self):
        """Add tags to the test"""

        if os.getenv('BUILD_HIP', 'NO') == 'YES':
            programming_model = "HIP"
        else:
            programming_model = "CUDA"
        if os.getenv('AMD', 'NO') == 'YES':
            gpu_brand = "AMD"
        else:
            gpu_brand = "NVIDIA"

        self.tags |= {
            # f'layout={self.layout}',
            # f'num_stations={self.num_stations}',
            # f'num_cycles={self.num_cycles}',
            # f'gridsize={self.gridsize}',
            # f'num_chans={self.num_chans}',
            # str(self.num_nodes),
            f'GPU_brand={gpu_brand}',
            f'Programming_Model={programming_model}',
        }

    # @run_after('setup')
    # def get_num_nodes(self):
    #     """Get number of nodes from total cores requested and number of cores per node"""
    #     self.num_cores = self.num_nodes * self.current_partition.processor.num_cpus
    #     self.tags |= {
    #         f'num_nodes={self.num_nodes}',
    #         f'num_cores={self.num_cores}',
    #     }

    # @run_after('setup')
    # def set_num_tasks_job(self):
    #     """This method sets tasks for the job. We use this to override the num_tasks set for
    #     reservation. Using this approach we can set num_tasks to job in a more generic way"""
    #     self.num_tasks = (self.current_partition.processor.num_cpus // 
    #                       self.current_partition.processor.num_cpus_per_core)
    #     self.tags |= {
    #         f'num_procs={self.num_tasks}'
    #     }

    # @run_after('setup')
    # def set_env_vars(self):
    #     """Set environment variables"""
    #     self.variables = {
    #         'LAYOUT_FILE': '.'.join([self.layout, 'txt']),
    #         'NR_CYCLES': str(self.num_cycles),
    #         'NR_STATIONS': str(self.num_stations),
    #         'GRIDSIZE': str(self.gridsize),
    #         'NR_CHANNELS': str(self.num_chans),
    #         'TOTAL_NR_TIMESTEPS': str(43200),
    #         'NR_TIMESTEPS': str(1800),
    #     }

    @run_after('setup')
    def add_launcher_options(self):
        """Set job launcher options"""
        if self.launcher_name == 'mpirun':
            # We are patching the launcher for mpirun/mpiexec to remove -np arg
            self.job.launcher.options = [
                '-np 1',
            ]
        elif self.launcher_name == 'mpiexec':
            # We are patching the launcher for mpirun/mpiexec to remove -np arg
            self.job.launcher.options = [
                '-n 1',
            ]
        elif self.launcher_name == 'srun':
            # Same goes with --nodes=<num_tasks_job> flag
            self.job.launcher.options = [
                '--ntasks=1',
            ]

    @run_before('run')
    def pre_launch(self):
        """Set prerun commands. It includes setting scratch directory and pre run commands from
        base class"""
        self.prerun_cmds += [
            f'cd {self.executable_path}',
        ]

        # Insert conda activate command at the front
        if os.getenv('CREATE_CONDA_ENV', 'YES') == 'YES':
            self.prerun_cmds.insert(0, f'conda activate {ENV_NAME}')
            self.prerun_cmds.insert(0, 'export PYTHONPATH=')

    @run_before('sanity')
    def set_sanity_patterns(self):
        """Set sanity patterns. Example stdout:
            degridder_v6:  305.36 ms, 5826.49 GFLOP/s,   16.23 GB/s,  359.08 FLOP/byte,  164.32 MVis/s,   94.46 W,   61.68 GFLOP/s/W,    1.74 Mvis/J 
        """
        self.sanity_patterns = sn.all([
            sn.assert_found('degridder_v6', self.stdout),
            #sn.assert_found('Total runtime', self.stderr),
            #sn.assert_found('Total throughput', self.stderr),
        ])

    @performance_function('s')
    def extract_time(self, kind='degridding'):
        """Performance extraction function for time. Sample stdout:
            degridder_v6:  305.36 ms, 5826.49 GFLOP/s,   16.23 GB/s,  359.08 FLOP/byte,  164.32 MVis/s,   94.46 W,   61.68 GFLOP/s/W,    1.74 Mvis/J 
        """

        return sn.extractsingle(rf'^\s*{kind}:\s+(?P<ms_value>\S+) ms,\s+(?P<gflop_value>\S+) GFLOP/s,\s+(?P<gb_value>\S+) GB/s,\s+(?P<ai_value>\S+) FLOP/byte,\s+(?P<mvis_value>\S+) MVis/s,\s+(?P<mvis_watt>\S+) W,\s+(?P<gflop_w_value>\S+) GFLOP/s/W,\s+(?P<mvis_j_value>\S+) MVis/J', self.stdout, 'ms_value', float)

    @performance_function('GFLOP/s')
    def extract_gflops(self, kind='degridding'):
        """Performance extraction function for time. Sample stdout:
            degridder_v6:  305.36 ms, 5826.49 GFLOP/s,   16.23 GB/s,  359.08 FLOP/byte,  164.32 MVis/s,   94.46 W,   61.68 GFLOP/s/W,    1.74 Mvis/J 
        """

        return sn.extractsingle(rf'^\s*{kind}:\s+(?P<ms_value>\S+) ms,\s+(?P<gflop_value>\S+) GFLOP/s,\s+(?P<gb_value>\S+) GB/s,\s+(?P<ai_value>\S+) FLOP/byte,\s+(?P<mvis_value>\S+) MVis/s,\s+(?P<mvis_watt>\S+) W,\s+(?P<gflop_w_value>\S+) GFLOP/s/W,\s+(?P<mvis_j_value>\S+) MVis/J', self.stdout, 'gflop_value', float)

    @performance_function('GB/s')
    def extract_gbs(self, kind='degridding'):
        """Performance extraction function for time. Sample stdout:
            degridder_v6:  305.36 ms, 5826.49 GFLOP/s,   16.23 GB/s,  359.08 FLOP/byte,  164.32 MVis/s,   94.46 W,   61.68 GFLOP/s/W,    1.74 Mvis/J 
        """

        return sn.extractsingle(rf'^\s*{kind}:\s+(?P<ms_value>\S+) ms,\s+(?P<gflop_value>\S+) GFLOP/s,\s+(?P<gb_value>\S+) GB/s,\s+(?P<ai_value>\S+) FLOP/byte,\s+(?P<mvis_value>\S+) MVis/s,\s+(?P<mvis_watt>\S+) W,\s+(?P<gflop_w_value>\S+) GFLOP/s/W,\s+(?P<mvis_j_value>\S+) MVis/J', self.stdout, 'gb_value', float)

    @performance_function('Flop/Byte')
    def extract_ai(self, kind='degridding'):
        """Performance extraction function for time. Sample stdout:
            degridder_v6:  305.36 ms, 5826.49 GFLOP/s,   16.23 GB/s,  359.08 FLOP/byte,  164.32 MVis/s,   94.46 W,   61.68 GFLOP/s/W,    1.74 Mvis/J 
        """

        return sn.extractsingle(rf'^\s*{kind}:\s+(?P<ms_value>\S+) ms,\s+(?P<gflop_value>\S+) GFLOP/s,\s+(?P<gb_value>\S+) GB/s,\s+(?P<ai_value>\S+) FLOP/byte,\s+(?P<mvis_value>\S+) MVis/s,\s+(?P<mvis_watt>\S+) W,\s+(?P<gflop_w_value>\S+) GFLOP/s/W,\s+(?P<mvis_j_value>\S+) MVis/J', self.stdout, 'ai_value', float)



    @performance_function('MVis/s')
    def extract_vis_thpt(self, kind='degridding'):
        """Performance extraction function for time. Sample stdout:
           degridder_v6:  305.36 ms, 5826.49 GFLOP/s,   16.23 GB/s,  359.08 FLOP/byte,  164.32 MVis/s,   94.46 W,   61.68 GFLOP/s/W,    1.74 Mvis/J 
        """

        return sn.extractsingle(rf'^\s*{kind}:\s+(?P<ms_value>\S+) ms,\s+(?P<gflop_value>\S+) GFLOP/s,\s+(?P<gb_value>\S+) GB/s,\s+(?P<ai_value>\S+) FLOP/byte,\s+(?P<mvis_value>\S+) MVis/s,\s+(?P<mvis_watt>\S+) W,\s+(?P<gflop_w_value>\S+) GFLOP/s/W,\s+(?P<mvis_j_value>\S+) MVis/J', self.stdout, 'mvis_value', float)

    @performance_function('W')
    def extract_watt(self, kind='degridding'):
        """Performance extraction function for time. Sample stdout:
           degridder_v6:  305.36 ms, 5826.49 GFLOP/s,   16.23 GB/s,  359.08 FLOP/byte,  164.32 MVis/s,   94.46 W,   61.68 GFLOP/s/W,    1.74 Mvis/J 
        """

        return sn.extractsingle(rf'^\s*{kind}:\s+(?P<ms_value>\S+) ms,\s+(?P<gflop_value>\S+) GFLOP/s,\s+(?P<gb_value>\S+) GB/s,\s+(?P<ai_value>\S+) FLOP/byte,\s+(?P<mvis_value>\S+) MVis/s,\s+(?P<watt>\S+) W,\s+(?P<gflop_w_value>\S+) GFLOP/s/W,\s+(?P<mvis_j_value>\S+) MVis/J', self.stdout, 'watt', float)

    @performance_function('GFLOP/s/W')
    def extract_glop_w(self, kind='degridding'):
        """Performance extraction function for time. Sample stdout:
           degridder_v6:  305.36 ms, 5826.49 GFLOP/s,   16.23 GB/s,  359.08 FLOP/byte,  164.32 MVis/s,   94.46 W,   61.68 GFLOP/s/W,    1.74 Mvis/J 
        """

        return sn.extractsingle(rf'^\s*{kind}:\s+(?P<ms_value>\S+) ms,\s+(?P<gflop_value>\S+) GFLOP/s,\s+(?P<gb_value>\S+) GB/s,\s+(?P<ai_value>\S+) FLOP/byte,\s+(?P<mvis_value>\S+) MVis/s,\s+(?P<mvis_watt>\S+) W,\s+(?P<gflop_w_value>\S+) GFLOP/s/W,\s+(?P<mvis_j_value>\S+) MVis/J', self.stdout, 'gflop_w_value', float)

    @performance_function('MVis/s')
    def extract_vis_j(self, kind='degridding'):
        """Performance extraction function for time. Sample stdout:
           degridder_v6:  305.36 ms, 5826.49 GFLOP/s,   16.23 GB/s,  359.08 FLOP/byte,  164.32 MVis/s,   94.46 W,   61.68 GFLOP/s/W,    1.74 Mvis/J 
        """

        return sn.extractsingle(rf'^\s*{kind}:\s+(?P<ms_value>\S+) ms,\s+(?P<gflop_value>\S+) GFLOP/s,\s+(?P<gb_value>\S+) GB/s,\s+(?P<ai_value>\S+) FLOP/byte,\s+(?P<mvis_value>\S+) MVis/s,\s+(?P<mvis_watt>\S+) W,\s+(?P<gflop_w_value>\S+) GFLOP/s/W,\s+(?P<mvis_j_value>\S+) MVis/J', self.stdout, 'mvis_j_value', float)

    @run_before('performance')
    def set_perf_patterns(self):
        """Set performance variables"""
        self.perf_variables = {
            'ms': self.extract_time(kind='degridder_v6'),
            'GFLOP/s': self.extract_gflops(kind='degridder_v6'),
            'GB/s': self.extract_gbs(kind='degridder_v6'),
            'FLOP/Byte': self.extract_ai(kind='degridder_v6'),
            'MVis/s': self.extract_vis_thpt(kind='degridder_v6'),
            'W': self.extract_watt(kind='degridder_v6'),
            'GFLOP/s/W': self.extract_glop_w(kind='degridder_v6'),
            'MVis/J': self.extract_vis_j(kind='degridder_v6'),
        }

    # @run_before('performance')
    # def set_reference_values(self):
    #     """Set reference perf values"""
    #     self.reference = {
    #         '*': {
    #             '*': (None, None, None, 's'),
    #             '*': (None, None, None, 'Mvis/s'),
    #         }
    #     }

