# Intel MPI Benchmarks (IMB)

This app contains IMB test. More details on benchmark, instructions to run are detailed in the [documentation](https://developer.skao.int/projects/ska-sdp-benchmark-tests/en/latest/content/benchmarks.html#intel-mpi-benchmarks). The `msglens` file in the `src/` folder is used to configure the message sizes in the benchmark tests.

The Jupyter notebook `IMB.ipynb` can be used to tabulate and latency, throughput and bandwidth metrics that are extracted during benchmarking tests. The notebook must be invoked from the `ska-sdp-benchmark-tests` root directory.
