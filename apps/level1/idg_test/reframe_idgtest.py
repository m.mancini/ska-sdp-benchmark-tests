# pylint: disable=C0301
"""
IDG Test
-------------------------

Context
~~~~~~~~~~~~~~~~~~~~~~~~

The image-domain gridder (IDG) is a new, fast gridder that makes w-term correction and 
a-term correction computationally very cheap. It performs extremely well on gpus. 
The source code is hosted on ASTRON
`GitLab <https://git.astron.nl/RD/idg>`_ repository and documentation
can be found `here <https://www.astron.nl/citt/IDG/quick-start.html>`__.

Test variables
~~~~~~~~~~~~~~~~~~~~~~~~

The test supports several runtime configurable variables:

- ``layout``: Antenna layout. Available options are ``SKA1_low`` and ``SKA1_mid``. (default is `SKA1_low`)
- ``num_cycles``: Number of major cycles (default: 10)
- ``num_stations``: Number of antenna stations (default: 100)
- ``gridsize``: Gridsize used for IDG (default: 8192)
- ``num_chans``: Number of frequency channels (default: 128)

This benchmark uses either ``SKA1_low`` or ``SKA1_mid`` antenna layout and generate random 
visibility data to do gridding and degridding. We use only one node and one GPU to run 
the benchmark and report various performance metrics. All these variables can be 
configured at the runtime which will be discussed in `:ref:idg usage`.

Environment variables
~~~~~~~~~~~~~~~~~~~~~~~~

By default, the test will create a ``conda`` environment and run inside it for the 
sake of isolation. This can be controlled using env variable ``CREATE_CONDA_ENV``. 
By setting it to ``NO``, the test WILL NOT create ``conda`` environment.

Similarly, the performance metrics are monitored using the `perfmon <https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon>`_ toolkit. If
the user does not want to monitor metrics, it can be achieved by setting 
``MONITOR_METRICS=NO``. 

.. _idg usage:

Usage
~~~~~~~~~~~~~~~~~~~~~~~~

The tests can be run using the following commands:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level1/idg_test/reframe_idgtest.py --run --performance-report

If we want to change the variables to non default values, we should use ``-S`` flag. For example, if we want to run only 5 major cycles and 64 frequency channels, use

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level1/idg_test/reframe_idgtest.py -S num_cycles=5 -S num_chans=64 --run --performance-report

Test class documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""
# pylint: enable=C0301

import os
from pathlib import Path

import reframe as rfm
import reframe.utility.sanity as sn  # pylint: disable=import-error
import reframe.utility.typecheck as typ  # pylint: disable=import-error
from modules.base_class import CompileOnlyBenchmarkBase
from reframe.core.builtins import fixture, run_before, variable, run_after, performance_function
from reframe.utility import udeps  # pylint: disable=import-error

from modules.reframe_extras import AppBase
from modules.reframe_extras import FetchSourcesBase
from modules.utils import filter_systems_by_env

# pylint: disable=C0116,E0401,E0602,E1101,W0201

# Global variables
# Define varibales that will be shared across tests here
# Each test will run in its own conda environment. We define name of that environment here
ENV_NAME = 'rfm_IdgTest'


class IdgTestDownload(FetchSourcesBase):
    """Fixture to fetch IDG source code"""
    
    descr = 'Fetch source code of IDG'
    sourcesdir = 'https://git.astron.nl/RD/idg.git'
    cnd_env_name = ENV_NAME


class IdgTestBuild(CompileOnlyBenchmarkBase):
    """IDG test compile test"""
    
    descr = 'Compile IDG test from sources'
    
    # Share resource from fixture
    idg_test_src = fixture(IdgTestDownload, scope='session')

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = [
            'idg-test',
        ]
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        self.maintainers = [
            'Mahendra Paipuri (mahendra.paipuri@inria.fr)'
        ]
        # Cross compilation is not possible on certain g5k clusters. We force
        # the job to be non-local so building will be on remote node
        if 'g5k' in self.current_system.name:
            self.build_locally = False
    
    @run_before('compile')
    def set_sourcedir(self):
        """Set source path based on dependencies"""
        self.sourcesdir = self.idg_test_src.stagedir
        
    @run_before('compile')
    def set_prebuild_cmds(self):
        """Make local lib dirs"""
        self.lib_dir = os.path.join(self.stagedir, 'local')
        self.prebuild_cmds = [
            f'mkdir -p {self.lib_dir}',
        ]

    @run_before('compile')
    def set_build_system_attrs(self):
        """Set build directory and config options"""
        self.build_system = 'CMake'
        self.build_system.builddir = os.path.join(self.stagedir, 'build')
        self.build_system.config_opts = [
            f'-DCMAKE_INSTALL_PREFIX={self.lib_dir}',
            '-DBUILD_LIB_CUDA=ON',
            '-DPERFORMANCE_REPORT=ON',
        ]
        self.build_system.max_concurrency = 8
        
    @run_before('compile')
    def set_postbuild_cmds(self):
        """Install libs"""
        self.postbuild_cmds = [
            'make install',
        ]

    @run_before('sanity')
    def set_sanity_patterns(self):
        """Set sanity patterns"""
        self.sanity_patterns = sn.assert_not_found('error', self.stderr)


@rfm.simple_test
class IdgTest(AppBase):
    """Main class of IDG benchmark tests"""

    time_limit = "0d2h0m0s"
    descr = 'IDG benchmarks for GPU performance.'

    # Variable to define antenna layout
    layout = variable(str, value='SKA1_low')

    # Variable to define number of cycles
    num_cycles = variable(int, value=10)
    
    # Variable to define number of stations
    num_stations = variable(int, value=100)
    
    # Variable to define gridsize
    gridsize = variable(int, value=8192)
    
    # Variable to define number of channels
    num_chans = variable(int, value=128)
    
    # Variable to define number of nodes
    # We always test on 1 node
    num_nodes = variable(int, value=1)

    idg_build_src = fixture(IdgTestBuild)

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = [
            'idg-test',
        ]
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        self.maintainers = [
            'Mahendra Paipuri (mahendra.paipuri@inria.fr)'
        ]
        self.exclusive_access = True
        
    @run_after('init')
    def check_vars(self):
        """Check test variables"""
        self.skip_if(
            self.layout not in ['SKA1_low', 'SKA1_mid'], msg='Layout must be '
            'either SKA1_low or SKA1_mid'
        )

    @run_before('run')
    def set_executable(self):
        """Set executable path and executable"""
        self.executable_path = Path(
            os.path.join(self.idg_build_src.stagedir, 'build', 'bin', 'cuda-generic.x')
        ).parent.absolute()
        self.sourcesdir = self.idg_build_src.stagedir
        self.executable = './cuda-generic.x'

    @run_after('init')
    def set_tags(self):
        """Add tags to the test"""
        self.tags |= {
            f'layout={self.layout}',
            f'num_stations={self.num_stations}',
            f'num_cycles={self.num_cycles}',
            f'gridsize={self.gridsize}',
            f'num_chans={self.num_chans}',
            str(self.num_nodes),
        }

    @run_after('setup')
    def get_num_nodes(self):
        """Get number of nodes from total cores requested and number of cores per node"""
        self.num_cores = self.num_nodes * self.current_partition.processor.num_cpus
        self.tags |= {
            f'num_nodes={self.num_nodes}',
            f'num_cores={self.num_cores}',
        }

    @run_after('setup')
    def set_num_tasks_job(self):
        """This method sets tasks for the job. We use this to override the num_tasks set for
        reservation. Using this approach we can set num_tasks to job in a more generic way"""
        self.num_tasks = (self.current_partition.processor.num_cpus // 
                          self.current_partition.processor.num_cpus_per_core)
        self.tags |= {
            f'num_procs={self.num_tasks}'
        }

    @run_after('setup')
    def set_env_vars(self):
        """Set environment variables"""
        self.variables = {
            'LAYOUT_FILE': '.'.join([self.layout, 'txt']),
            'NR_CYCLES': str(self.num_cycles),
            'NR_STATIONS': str(self.num_stations),
            'GRIDSIZE': str(self.gridsize),
            'NR_CHANNELS': str(self.num_chans),
            'TOTAL_NR_TIMESTEPS': str(43200),
            'NR_TIMESTEPS': str(1800),
        }

    @run_after('setup')
    def add_launcher_options(self):
        """Set job launcher options"""
        if self.launcher_name == 'mpirun':
            # We are patching the launcher for mpirun/mpiexec to remove -np arg
            self.job.launcher.options = [ 
                '-np 1',
            ]
        elif self.launcher_name == 'mpiexec':
            # We are patching the launcher for mpirun/mpiexec to remove -np arg
            self.job.launcher.options = [
                '-n 1',
            ]
        elif self.launcher_name == 'srun':
            # Same goes with --nodes=<num_tasks_job> flag
            self.job.launcher.options = [
                '--ntasks=1',
            ]

    @run_before('run')
    def pre_launch(self):
        """Set prerun commands. It includes setting scratch directory and pre run commands from
        base class"""
        self.prerun_cmds += [
            f'cd {self.executable_path}',
        ]

    @run_before('sanity')
    def set_sanity_patterns(self):
        """Set sanity patterns. Example stdout:

        .. code-block:: text

            >>> Total runtime
            gridding:   6.5067e+02 s
            degridding: 1.0607e+03 s
            fft:        3.5437e-01 s
            get_image:  6.5767e+00 s
            imaging:    2.0073e+03 s

            >>> Total throughput
            gridding:   3.12 Mvisibilities/s
            degridding: 1.91 Mvisibilities/s
            imaging:    1.01 Mvisibilities/s

        """
        self.sanity_patterns = sn.all([
            sn.assert_found('Total runtime', self.stderr),
            sn.assert_found('Total throughput', self.stderr),
        ])

    @performance_function('s')
    def extract_time(self, kind='gridding'):
        """Performance extraction function for time. Sample stdout:


        .. code-block:: text

            >>> Total runtime
            gridding:   7.5473e+02 s
            degridding: 1.1090e+03 s
            fft:        3.5368e-01 s
            get_image:  7.2816e+00 s
            imaging:    1.8899e+03 s
            
        """
        return sn.extractsingle(rf'^{kind}:\s+(?P<value>\S+) s', self.stderr, 'value', float)
    
    @performance_function('Mvisibilities/s')
    def extract_vis_thpt(self, kind='gridding'):
        """Performance extraction function for visibility throughput. Sample stdout:


        .. code-block:: text

            >>> Total throughput
            gridding:   2.69 Mvisibilities/s
            degridding: 1.83 Mvisibilities/s
            imaging:    1.07 Mvisibilities/s

        """
        return sn.extractsingle(rf'^{kind}:\s+(?P<value>\S+) Mvisibilities/s', self.stderr, 'value', float)

    @run_before('performance')
    def set_perf_patterns(self):
        """Set performance variables"""
        self.perf_variables = {
            'gridding s': self.extract_time(),
            'degridding s': self.extract_time(kind='degridding'),
            'fft s': self.extract_time(kind='fft'),
            'get_image s': self.extract_time(kind='get_image'),
            'imaging s': self.extract_time(kind='imaging'),
            'gridding Mvis/s': self.extract_vis_thpt(),
            'degridding Mvis/s': self.extract_vis_thpt(kind='degridding'),
            'imaging Mvis/s': self.extract_vis_thpt(kind='imaging'),
        }

    @run_before('performance')
    def set_reference_values(self):
        """Set reference perf values"""
        self.reference = {
            '*': {
                '*': (None, None, None, 's'),
                '*': (None, None, None, 'Mvis/s'),
            }
        }
