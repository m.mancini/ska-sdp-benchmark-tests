# CUDA NIFTY gridder benchmark test

This simple package benchmarks CUDA NIFTY gridder using synthetic SKA1 MID dataset for different image sizes and frequency channels on NVIDIA GPUs. If the node has multiple GPUs, the benchmark will take advantage of all the GPUs by running multiple benchmarks in parallel. It reports the wall time for predict and invert functions for a given image size and for a range of frequency channels.

UVW coverage is generated based on SKA1 MID antennas location for an observation period of 8 hours. The integration time can be configured at the runtime of the benchmark test. Visibilities are generated randomly with a uniform distribution between [-0.5, 0.5].

## Installation

Currently, the package is provided as part of [SDP Benchmark tests](https://gitlab.com/ska-telescope/sdp/ska-sdp-benchmark-tests) repository. The sources of the package can be found at `apps/level1/cuda_nifty/src` folder in the repository. Once these sources are fetched, the installation is straight-forward. Assuming you are at the root of the package, simply run

```
python setup.py install
```

This will install all build-time and run-time dependencies and `cng_test` package within your python environment.

## Usage

The package is provided with the executable script `cngtest` which launches the benchmark tests. Available CLI options can be seen using `cngtest -h` which will emit following output

```
usage: cngtest [-h] [--scale SCALE] [--interval INTERVAL] [--min_nchan MIN_NCHAN] [--max_nchan MAX_NCHAN] [--precision {sp,dp}]
               [--epsilon EPSILON] [-v]

CUDA NIFTY gridder benchmark test

optional arguments:
  -h, --help            show this help message and exit
  --scale SCALE         Size of the image as multiple of 1024 (Default is 1)
  --interval INTERVAL   Integration interval in sec (Default is 1800)
  --min_nchan MIN_NCHAN
                        Minimum number of channels as power of 2 (Default is 0)
  --max_nchan MAX_NCHAN
                        Maxmimum number of channels as power of 2 (Default is 11)
  --precision {sp,dp}   Precision to be used for visibilities data arrays (Default is sp)
  --epsilon EPSILON     Accuracy of (de)gridding computations (Default is 1e-5)
  -v, --verbose         Enable verbose mode. Display debug messages
```

All the options are self-explanatory. The test can be run from command line using `cngtest` command at minimum. If we want to run a test using 16k image, we can use `cngtest --scale 16` which will emit following output:

```
-----------------------------------------------------------------------------------------
Image size:                            16384 x 16384
Pixel size (in degrees):               4.099e-05
Field of view (in degrees):            6.716e-01
Minimum frequency:                     1.300e+09
Maximum frequency:                     1.360e+09
Number of baselines:                   312048
Integration interval (in sec):         1800
Precision:                             sp
Accuracy:                              1e-05
==========================================================================================
            CUDA NIFTY Benchmark results using synthetic SKA1 MID dataset
==========================================================================================
Image size        # Channels        # Visibilities    Invert time [s]   Predict time [s]  
16384             1                 312048            10.22508          8.30572           
16384             2                 624096            10.88928          8.69217           
16384             4                 1248192           10.63302          8.76751           
16384             8                 2496384           10.79545          8.75861           
16384             16                4992768           11.2209           8.80125           
16384             32                9985536           11.36171          8.8448            
16384             64                19971072          12.03696          9.28774           
16384             128               39942144          12.42435          9.345             
16384             256               79884288          12.84528          9.86815           
16384             512               159768576         16.64954          11.06888          
16384             1024              319537152         23.98354          13.2022           
-----------------------------------------------------------------------------------------
```

The above test is made on Tesla V100 with 32 GiB of memory. If the runtime memory requirements exceeds available GPU memory, some error messages are printed at the top of the `stdout`. The benchmark will report only tests that have finished successfully. If there are some failed tests due to lack of memory, the benchmark will print a message `Some tests have failed` at the end.
