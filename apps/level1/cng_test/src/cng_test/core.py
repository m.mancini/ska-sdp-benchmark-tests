"""Main test file"""

import os
import time
import logging
from multiprocessing import Pool
import numpy as np

import cuda_nifty_gridder as cng

from cng_test.uvw_coverage import generate_ska1_mid_uv_coverage
from cng_test.utils import get_num_gpus
from cng_test.utils import pretty_print_results

# Global variables
C_M_S = 299792458.0  # Speed of light in m/s
BASE_SIZE = 1024  # Base size of image. Will be multiplied by scale
START_FREQ = 1.3e9
END_FREQ = 1.36e9
# Precision data types
DTYPES = {
    'vis': {
        'sp': np.complex64,
        'dp': np.complex128,
    },
    'img': {
        'sp': np.float32,
        'dp': np.float64,
    }
}

# Set logger
_log = logging.getLogger(__name__)

# Let's take the randomness out of random numbers (for reproducibility)
np.random.seed(0)


def generate_vis_data(precision, nchan, uvw):
    """
    Generate synthetic (random) visibility data for the given uvw coverage and random image with
    given size and frequencies

    :param precision: Precision of visibility data
    :param nchan: Number of channels
    :param uvw: UVW coverage coordinates
    :returns: Array of visibility
    """

    # Get number of baselines and number of time intervals
    nrows, _ = uvw.shape

    _log.debug('Generating visibility data for %d rows and %d channel(s)'
               % (nrows, nchan))

    vis = ((np.random.rand(nrows, nchan).astype(DTYPES['vis'][precision]) - 0.5) +
           1j * (np.random.rand(nrows, nchan).astype(DTYPES['vis'][precision]) - 0.5))

    # Convert vis array into contiguous array
    vis = np.ascontiguousarray(vis)

    return vis


def generate_freq_data(nchan):
    """
    Generate frequency channels based on number of frequencies.

    :param nchan: Number of channels
    :returns: Array of frequency
    """

    # Frequency array
    if nchan > 1:
        frequency = np.linspace(START_FREQ, END_FREQ, nchan)
    else:
        frequency = np.array([START_FREQ])

    _log.debug('%d channels are created between %.2e and %.2e frequency range' %
               (nchan, START_FREQ, END_FREQ))

    return frequency


def generate_dirty_image(precision, npixels):
    """
    Generate random dirty image with given size

    :param precision: Precision of image data
    :param npixels: Number of pixels in the image
    :returns: Image array
    """
    return np.random.rand(npixels, npixels).astype(DTYPES['img'][precision]) - 0.5


def estimate_pixel_size(uvw):
    """
    Estimate pixel size based on uvw data
    :param uvw: UVW covergae
    :return: Pixel size of the image
    """

    # Min wavelength and max baseline
    min_wavelength = C_M_S / END_FREQ
    maximum_baseline = np.max(uvw) / min_wavelength

    synthesized_beam = 1.0 / maximum_baseline
    # Stolen from here -> https://gitlab.com/ska-telescope/external/rascil/-/blob/master/rascil/processing_components/simulation/testing_support.py#L1057
    oversampling_synthesised_beam = 2.0

    # Pixel size in radians
    pixsize = synthesized_beam / oversampling_synthesised_beam
    _log.debug('Estimated pixel size is %.2e degrees' % pixsize)

    return pixsize


def run_test(gpu_num, scale, precision, niter, pixsize, epsilon, nchan, uvw):
    """
    Main function to run the benchmark test

    :param gpu_num: GPU device id to run the test
    :param scale: Scale the size of base image
    :param precision: Precision of image data
    :param niter: Number of repetitions of each test
    :param pixsize: Size of pixel
    :param epsilon: Accuracy of (de)gridder
    :param nchan: Number of frequency array
    :param uvw: UVW coverage
    :returns:
       - Number of pixels
       - number of channels
       - number of visibiliies
       - invert time
       - predict time
       - test status
    """

    # Affine process to given GPU
    os.environ['CUDA_VISIBLE_DEVICES'] = str(gpu_num)

    # Generate synthetic visibility data
    vis = generate_vis_data(precision=precision, nchan=nchan, uvw=uvw)
    nrow, nchan = vis.shape

    # Total number of visibilities
    nvis = nrow * nchan

    # Generate dirty image
    npixel = scale * BASE_SIZE
    dirty_image = generate_dirty_image(precision, npixel)

    # Generate frequency channels
    frequency = generate_freq_data(nchan)

    # List to store times of each test
    results = []

    # Repeat each test niter times
    for _ in range(niter):
        try:
            # Invert test
            _log.debug('Starting invert test....')
            start_time = time.time()
            dirty = cng.ms2dirty(uvw=uvw, freq=frequency, ms=vis, weight=None, npix_x=npixel, npix_y=npixel,
                                 pixsize_x_rad=pixsize, pixsize_y_rad=pixsize, dummy1=0, dummy2=0, epsilon=epsilon,
                                 do_wstacking=True)
            invert_time = time.time() - start_time
            _log.debug('Invert test finished in %.3f sec' % invert_time)

            # Predict test
            _log.debug('Starting predict test....')
            start_time = time.time()
            vis_predict = cng.dirty2ms(uvw=uvw, freq=frequency, dirty=dirty_image, weight=None,
                                       pixsize_x_rad=pixsize, pixsize_y_rad=pixsize, dummy1=0, dummy2=0,
                                       epsilon=epsilon, do_wstacking=True)
            predict_time = time.time() - start_time
            _log.debug('Predict test finished in %.3f sec' % predict_time)

            # Test for adjointness
            _log.debug('Testing for adjointness...')
            adj1 = np.vdot(dirty, dirty_image)
            adj2 = np.vdot(vis_predict, vis).real
            adj_error = np.abs(adj1 - adj2) / np.maximum(np.abs(adj1), np.abs(adj2))
            _log.debug(f'Adjoint error is %.2e' % adj_error)

            # Append times to results list
            results.append([invert_time, predict_time, adj_error])
        except Exception as err:
            #print(err)
            _log.error(err)
            return scale, nchan, nvis, 0, 0, 0, 'Fail'

    # Get average times and error
    avg_times = np.around(np.mean(np.array(results), axis=0), decimals=5)

    return npixel, nchan, nvis, avg_times[0], avg_times[1], avg_times[2], 'Pass'


def run_scaling_test(scale, min_nchan, max_nchan, precision, niter, pixsize, epsilon, uvw):
    """
    Main function to run scaling tests

    :param scale: Scale the size of base image
    :param min_nchan: Minimum number of channels as power of 2
    :param max_nchan: Maximum number of channels as power of 2
    :param precision: Precision of image data
    :param niter: Number of repetitions of each test
    :param pixsize: Size of pixel
    :param epsilon: Accuracy of (de)gridder
    :param uvw: UVW coverage
    :return: List of results
    :rtype: list
    """

    # Generate parameterisation of variable
    cases = [1 << i for i in range(min_nchan, max_nchan)]

    # Generate all arguments list
    arguments = [[ip, scale, precision, niter, pixsize, epsilon, p, uvw]
                 for ip, p in enumerate(cases)]

    # Get number of nvidia gpus
    num_gpus = get_num_gpus()

    # Add header to metrics
    metrics = [['Image size', '# Channels', '# Visibilities', 'Invert time [s]',
                'Predict time [s]', 'Adjoint error', 'Pass']]

    # Spawn as many processes as number of GPUs
    with Pool(processes=num_gpus) as pool:
        results = pool.starmap(run_test, arguments)

    # Add results to metrics list
    metrics.extend(results)

    return metrics


def cng_test_entrypoint(config):
    """Main entry point to the test"""

    # Add frequency range to config
    config['min_freq'] = START_FREQ
    config['max_freq'] = END_FREQ

    # Generate uvw coverage
    _log.debug('Generating UVW coverage....')
    start_time = time.time()
    uvw = generate_ska1_mid_uv_coverage(interval=config['interval'])
    uv_data_time = time.time() - start_time
    config['nbaselines'] = uvw.shape[0]
    _log.debug('UVW coverage data generated in %.3f sec' % uv_data_time)

    # Estimate pixel size
    _log.debug('Estimating pixel size....')
    pixsize = estimate_pixel_size(uvw=uvw)
    config['pixsize'] = pixsize
    _log.debug('Finished estimating pixel size')

    # Scalability tests
    _log.debug('Running scalability tests channels....')
    results = run_scaling_test(scale=config['scale'], min_nchan=config['min_nchan'],
                               max_nchan=config['max_nchan'], niter=config['niter'],
                               precision=config['precision'], pixsize=pixsize,
                               epsilon=config['epsilon'], uvw=uvw)
    _log.debug('Scalability tests finished')

    # Print results
    pretty_print_results(config, results)
