"""Argument parser"""

import argparse


def parse_args():
    """
    Parse command line arguments

    :returns: Dict with command line arguments
    """

    parser = argparse.ArgumentParser(
        description='CUDA NIFTY gridder benchmark test', fromfile_prefix_chars='@'
    )

    parser.add_argument(
        '--scale',
        dest='scale',
        type=int,
        default=1,
        help='Size of the image as multiple of 1024 (Default is 1)'
    )
    parser.add_argument(
        '--interval',
        dest='interval',
        type=float,
        default=1800,
        help='Integration interval in sec (Default is 1800)'
    )
    parser.add_argument(
        '--min_nchan',
        dest='min_nchan',
        type=int,
        default=0,
        help='Minimum number of channels as power of 2 (Default is 0)'
    )
    parser.add_argument(
        '--max_nchan',
        dest='max_nchan',
        type=int,
        default=11,
        help='Maxmimum number of channels as power of 2 (Default is 11)'
    )
    parser.add_argument(
        '--niter',
        dest='niter',
        type=int,
        default=10,
        help='Number of iterations to repeat each test (Default is 10)'
    )
    parser.add_argument(
        '--precision',
        dest='precision',
        type=str,
        default='sp',
        choices=['sp', 'dp'],
        help='Precision to be used for visibilities data arrays (Default is sp)',
    )
    parser.add_argument(
        '--epsilon',
        dest='epsilon',
        type=float,
        default=1e-5,
        help='Accuracy of (de)gridding computations (Default is 1e-5)',
    )
    parser.add_argument(
        '-v', '--verbose',
        dest='verbose',
        default=False,
        action='store_true',
        help='Enable verbose mode. Display debug messages',
    )

    return vars(parser.parse_args())
