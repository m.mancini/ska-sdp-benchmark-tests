#!/usr/bin/env python3
"""
CUDA NIFTY Benchmark test using simulated dataset for SKA1 MID config. The script comes with a
config file that contains the coordinates of the SKA1 MID antennas locations. We use this coordinate
file and by assuming an integration time we estimate the baselines. Random visibility data is
generated for these baselines and CUDA NIFTY (de)gridder is tested against this data.

Using such an approach, we can parameterise the number of channels, image size to run several
benchmark tests.

To determine the accuracy of the (de)gridding functions, we should compare the CUDA NIFTY output
with DFT output which can be quite expensive, especially for large images. So we use the adjoint
test to determine the consistency of gridding and degridding functions. More information on
adjoint test can be found at https://www.aanda.org/articles/aa/pdf/2021/02/aa39723-20.pdf

All the functions that are used to generate the baselines are taken from RASCIL.
https://gitlab.com/ska-telescope/external/rascil
"""

import sys
import os

# Add current directory to PYTHONPATH
try:
    parent_dir = os.path.dirname(__file__)
    sys.path.index(parent_dir)
except ValueError:
    sys.path.insert(0, parent_dir)

try:
    import cng_test
except ImportError as e:
    print(sys.executable, sys.version)
    print('sys.path:')
    for i in range(len(sys.path)):
        print('%2d) %s' % (i + 1, sys.path[i]))
    raise


sys.exit(
    cng_test.main()
)
