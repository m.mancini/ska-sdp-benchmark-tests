# pylint: disable=C0301
"""
CUDA NIFTY gridder performance benchmark
------------------------------------------------------

Context
~~~~~~~~~~~~~~~~~~~~~~~~

`CUDA NIFTY Gridder (CNG) <https://gitlab.com/ska-telescope/sdp/ska-gridder-nifty-cuda>`_ is a
CUDA implementation of `NIFTY gridder <https://gitlab.mpcdf.mpg.de/ift/nifty_gridder>`_ to
(de)grid interferometric data using improved w-stacking algorithm.

In this test, we are interested in performance of CNG on different GPU devices. In order to stress
the gridder, we use SKA1 MID synthetic dataset with configurable image size. More details on the
design of the benchmark can be found in ``src/`` folder.

.. note::
  The benchmark test uses visibility data that is randomly generated for a given uvw coverage. It is
  very expensive to do a DFT on this data to estimate the accuracy of the CNG. Hence, no accuracy
  tests are performed within this benchmark.

Test configuration
~~~~~~~~~~~~~~~~~~~~~~~~

The tests can be configured to change the minimum and maximum number of frequency channels that
will be used in the benchmark. Similarly, the image size can also be configured at the runtime.
Available variables that can be configured at runtime are

- ``min_chans``: Minimum number of frequency channels as power of 2 (default: 0)
- ``max_chans``: Maximum number of frequency channels as power of 2 (default: 11)
- ``img_size``: Image size as multiple of 1024 (default: 8)

With default variables, the benchmark will test on a image size of 8192 x 8192 pixels using 1 to
1024 frequency channels. They can be configured from CLI using ``-S`` flag which will be shown in
:ref:`cng usage`.

.. _cng usage:

Usage
~~~~~~~~~~~~~~~~~~~~~~~~

The test can be run using following commands.

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level1/cng_test/reframe_cngtest.py --run --performance-report

To run using 16k image and till 4096 frequency channels, use ``-S`` option as

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level1/cng_test/reframe_cngtest.py -S img_size=16 -t max_chans=13 --run --performance-report


Test class documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""
# pylint: enable=C0301

import os

import reframe as rfm
import reframe.utility.sanity as sn  # pylint: disable=import-error
import reframe.utility.osext as osext  # pylint: disable=import-error
from modules.base_class import RunOnlyBenchmarkBase

from modules.utils import filter_systems_by_env

from modules.reframe_extras import AppBase
from reframe.core.builtins import fixture, run_after, performance_function, run_before, variable
import modules.conda_env_manager as cem

# pylint: disable=C0116,E0401,E0602,E1101,W0201

# Global variables
# Define varibales that will be shared across tests here
# Each test will run in its own conda environment. We define the name of that environment here
ENV_NAME = 'rfm_CngTest'


class CngBuildTest(RunOnlyBenchmarkBase):
    descr = "Installing CNG Benchmark"

    def __init__(self):
        super().__init__()
        self.executable = "echo noop"
        self.maintainers = [
            'Manuel Stutz (manuel.stutz@fhnw.ch)'
        ]
        self.env_manager = cem.CondaEnvManager(ENV_NAME)

    @run_after('setup')
    def activate_conda_env(self):
        self.env_manager.create()
        self.prerun_cmds = self.env_manager.emit_conda_activate_cmds()

    @run_after('setup')
    def set_source_path(self):
        """Get source path attribute"""
        self.source_path = self.stagedir

    @run_after('setup')
    def install_cng_test(self):
        """Installs cng_test in conda env"""
        self.env_manager.install_pip("numpy~=1.21.4")
        self.prerun_cmds += [
            'pip install -r requirements.txt',
            'pip install .',
        ]

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.all([
            sn.assert_not_found('Error', self.stderr)
        ])


@rfm.simple_test
class CngTest(AppBase):
    """CUDA NIFTY Gridder (CNG) performance tests main class"""

    descr = 'CUDA NIFTY Gridder (CNG) benchmark using SKA1 MID dataset'
    cng_src = fixture(CngBuildTest, scope="environment")

    # Variable to define number of nodes
    num_nodes = variable(int, value=1)

    # Variable to set image size (8k image)
    img_size = variable(int, value=8)

    # Variable to set minimum number of frequency channels (as power of 2)
    min_chans = variable(int, value=0)

    # Variable to set maximum number of frequency channels (as power of 2)
    max_chans = variable(int, value=11)

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = [
            'cng-test',
        ]
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        self.maintainers = [
            'Mahendra Paipuri (mahendra.paipuri@inria.fr)'
        ]
        self.exclusive_access = True
        self.time_limit = '0d1h00m0s'
        self.freq_chans = [1 << i for i in range(self.min_chans, self.max_chans)]
        self.min_num_chan = self.freq_chans[0]
        self.env_manager = cem.CondaEnvManager(ENV_NAME)

    @run_after('init')
    def set_conda_prerun_cmds(self):
        """Emit conda env prerun commands"""
        self.prerun_cmds += self.env_manager.emit_conda_activate_cmds()

    @run_after('setup')
    def set_tags(self):
        """Add tags to the test"""
        self.tags |= {
            f'num_nodes={self.num_nodes}',
            f'image_size={self.img_size}',
            f'min_chans={self.min_chans}',
            f'max_chans={self.min_chans}',
            f'num_gpus={self.current_partition.devices[0].num_devices}',
        }

    @run_before('run')
    def set_launcher_opts(self):
        """Set job launcher options"""
        if self.job.launcher.registered_name == 'mpirun':
            self.job.launcher.options = ['-np 1']
        elif self.job.launcher.registered_name == 'mpiexec':
            self.job.launcher.options = ['-genvall -n 1']
        elif self.job.launcher.registered_name == 'srun':
            self.job.launcher.options = ['--ntasks=1 --ntasks-per-node=1']

    @run_before('run')
    def set_executable(self):
        """Set executable name"""
        self.executable = 'cngtest'
        self.executable_opts = [
            f'--scale {self.img_size}',
            f'--min_nchan {self.min_chans}',
            f'--max_nchan {self.max_chans}',
        ]

    @run_before('sanity')
    def set_git_commit_tag(self):
        """Override git tag method from base class"""
        # We do this as CUDA NIFTY gridder is available only after installation of cng_test
        git_ref = osext.git_repo_hash(
            short=False, wd=os.path.join(self.stagedir, 'src', 'ska-gridder-nifty-cuda')
        )
        self.tags |= {f'git={git_ref}'}

    @run_before('sanity')
    def set_sanity_patterns(self):
        """Set sanity patterns. Example stdout:

        .. code-block:: text

            All tests have successfully finished

        """
        self.sanity_patterns = sn.all(
            [sn.assert_not_found('# Some tests have failed', self.stdout),
             sn.assert_not_found('# All tests have failed', self.stdout),
             sn.assert_not_found('ERROR', self.stdout),
             sn.assert_found('CUDA NIFTY Benchmark results using synthetic SKA1 MID dataset', self.stdout)]
        )

    def parse_stdout(self, num_chan, perf):
        """Read stdout file to extract perf variables"""
        ind = {
            'vis': 1,
            'predict': 2,
            'invert': 3,
        }
        with open(self.stdout.evaluate()) as f:
            for line in f:
                if '# Channels        ' in line:
                    while '# End of table' not in line:
                        line = next(f)
                        row = line.split('#')[-1].split()
                        if str(num_chan) in row:
                            if perf == 'vis':
                                met = int(row[ind[perf]])
                            else:
                                met = float(row[ind[perf]])
                            return met
        return 0

    @performance_function('s')
    def extract_time(self, num_chan=None, perf='invert'):
        """Performance function to extract (de)gridding times"""
        if num_chan is None:
            num_chan = self.min_num_chan
        return self.parse_stdout(num_chan, perf)

    @performance_function('vis')
    def extract_vis(self, num_chan=None, perf='vis'):
        """Performance function to extract number of visibilities"""
        if num_chan is None:
            num_chan = self.min_num_chan
        return self.parse_stdout(num_chan, perf)

    @run_before('performance')
    def set_perf_patterns(self):
        """Set performance variables. Sample stdout:

        .. code-block:: text

            ---------------------------------------------------------------------------------------
            # Image size:                            4096 x 4096
            # Pixel size (in degrees):               4.099e-05
            # Field of view (in degrees):            1.679e-01
            # Minimum frequency:                     1.300e+09
            # Maximum frequency:                     1.360e+09
            # Number of baselines:                   312048
            # Integration interval (in sec):         1800
            # Precision:                             sp
            # Accuracy:                              1e-05
            # Number of iterations:                  10
            =======================================================================================
                        CUDA NIFTY Benchmark results using synthetic SKA1 MID dataset
            =======================================================================================
            # # Channels        # Visibilities    Invert time [s]   Predict time [s]
            # 1                 312048            0.30428           0.09089
            # 2                 624096            0.33928           0.09925
            # 4                 1248192           0.34887           0.10189
            # 8                 2496384           0.38257           0.11387
            # 16                4992768           0.43269           0.14284
            # 32                9985536           0.53047           0.17708
            # 64                19971072          0.73875           0.26098
            # 128               39942144          1.26618           0.44106
            # 256               79884288          2.17768           0.74469
            # 512               159768576         4.34335           1.34687
            # 1024              319537152         8.66043           2.64405
            ---------------------------------------------------------------------------------------
            # End of table
            # All tests have successfully finished

        """
        self.perf_variables = {
            f'vis_{self.min_num_chan}': self.extract_vis(),
            f'invert_{self.min_num_chan}': self.extract_time(),
            f'predict_{self.min_num_chan}': self.extract_time(perf='predict'),
        }
        for perf in ['invert', 'predict', 'vis']:
            for num_chan in self.freq_chans[1:]:
                if perf in ['invert', 'predict']:
                    methd_name = 'time'
                else:
                    methd_name = perf
                self.perf_variables[f'{perf}_{num_chan}'] = \
                    getattr(self, f'extract_{methd_name}')(num_chan, perf)

    @run_before('performance')
    def set_reference_values(self):
        """Set reference perf values"""
        self.reference = {
            '*': {
                '*': (None, None, None, '*'),
            }
        }
