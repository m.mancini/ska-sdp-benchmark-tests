"""Conda environment manager for SDP benchmark tests"""

import subprocess
import os
import logging
import json
from pathlib import Path
import traceback

# Global variables
PERFMON_URL = 'git+https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon.git@v2.0.0#egg=ska-sdp-perfmon'

# INFO ERROR DEBUG WARNING
logging.basicConfig(level=logging.ERROR)

LOGGER = logging.getLogger(__name__)


class CondaEnvManager:

    def __init__(self, env_name, install_perfmon=(os.getenv('MONITOR_METRICS', 'YES') == 'YES'), py_ver='3.8'):
        self.env_name = env_name
        self.env_loc = os.path.join(
            subprocess.run(f'{self.emit_conda_init_cmds()} echo $(conda info --base)', shell=True,
                           stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True).stdout.strip(), 'envs',
            self.env_name)
        logging.debug(f'{self.env_name} {self.env_loc}')
        self.py_ver = py_ver
        self.install_perfmon = install_perfmon
        self.download_directory = os.path.join(Path(os.path.dirname(__file__)).parent, 'cache', env_name)
        # self.install_directory=os.path.join(os.path.dirname(__file__).parent, 'external',env_name)
        # os.makedirs(self.install_directory, exist_ok=True)

    def emit_conda_init_cmds(self):
        """
        This function emits the command to initialize conda.
        Necessary for all interaction with conda environment (add at the begging of each command)
        """
        return '. $(conda info --base)/etc/profile.d/conda.sh;'
        # return '. $(conda info --root)/etc/profile.d/conda.sh;' # root renamed in base for new version

    def emit_conda_activate_cmds(self):
        """
        This function uses the emit_conda_init_cmds function and activate the environment.
        Necessary to use conda environment in the subprocess module.
        """
        return self.emit_conda_init_cmds() + f' conda activate {self.env_name};'

    def is_loaded(self):
        """
        This function to check if the conda environment can be loaded.
        """
        try:
            logging.debug(f'{self.env_name} conda environment is loaded.')
            p = subprocess.run(f'{self.emit_conda_activate_cmds()} conda env list | grep "*"',
                               shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True, text=True)

            current_env = p.stdout.split('*')[0].strip()
            logging.debug(f'{self.env_name} compared with {current_env}')
            return self.env_name == current_env
        except:
            logging.debug(f'{self.env_name} conda environment is not loaded.')
            return False

    def is_exist_empty_env_folder(self):
        """
        Identify if the conda environment folder exists and is empty.
        """
        return os.path.isdir(self.env_loc) and len(os.listdir(self.env_loc)) == 0

    def remove_conda_env_folder(self):
        """
        Remove the conda environment folder.
        """
        try:
            os.rmdir(self.env_loc)
            logging.debug(f'Remove the environment folder {self.env_name}.')
            return True
        except OSError as e:
            logging.debug(f'We can\'t remove the environment folder {self.env_name}.')
            logging.debug(f'Error: {e.errno}')
            return False

    def remove(self):
        """
        Remove the conda environment. If the folder exist and is empty, we just remove the env folder.
        """
        if self.is_exist_empty_env_folder():
            return self.remove_conda_env_folder()
        else:
            if not self.does_exist() and not os.path.isdir(self.env_loc):
                logging.debug(f'Environment {self.env_name} and its folder don\'t exist.')
                return False
            else:
                try:
                    # return code not safe
                    p = subprocess.run(f'{self.emit_conda_init_cmds()} conda env remove -n {self.env_name}', shell=True,
                                       check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
                    if not self.does_exist() and not self.is_exist_empty_env_folder():
                        logging.debug(
                            f'Env exists : {self.does_exist()}, Env folder exists: {self.is_exist_empty_env_folder()}')
                        logging.debug(f'Remove the environment {self.env_name} automatically.')
                        return True  # not bool(p.returncode)
                    else:
                        logging.debug(f'Can\'t remove the environment {self.env_name} automatically.')
                        return self.remove_conda_env_folder()
                except OSError as e:
                    logging.debug(f'Can\'t remove the environment {self.env_name}.')
                    logging.debug(f'Error: {e.errno}')
                    return False

    def create(self):
        """
        Create the conda environment if it exists.
        """
        if not self.does_exist():
            try:
                logging.debug(f'Create the environment {self.env_name}.')
                p = subprocess.run(
                    f'{self.emit_conda_init_cmds()} conda create --prefix={self.env_loc} python={self.py_ver} -y',
                    shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
                if self.install_perfmon:
                    self.install_pip(f'\"{PERFMON_URL}\"', local=True, options="--no-cache-dir --force-reinstall")
                return not bool(p.returncode)
            except Exception as e:
                logging.debug(f'Can\'t create the environment {self.env_name}.')
                logging.error(f"Error: {e}")
                return False
        else:
            logging.debug(f'Environment {self.env_name} exists.')
            return False

    def does_exist(self):
        """
        Check if the conda environment exists.
        """
        logging.debug(f'Check if the environment {self.env_name} exists.')
        p = subprocess.run(f'{self.emit_conda_init_cmds()} conda env list --json',
                           shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True, text=True)
        js = json.loads(p.stdout)

        p = subprocess.run(f'{self.emit_conda_init_cmds()} conda activate ; conda env list | grep "*"',
                           shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True, text=True)
        prefix = p.stdout.split('*')[1].strip()
        prefix = prefix.split('/envs/')[0] + '/envs/' + self.env_name
        return prefix in js['envs']

    def install_conda(self, package, options=""):
        """
        Install a conda package in the conda environment.
        :param package: package to install
        :type package: str
        :param options: conda options for the installation
        :type options: str
        """
        try:
            logging.debug(f'Install the conda package {package} for the environment {self.env_name}.')
            p = subprocess.run(
                f'{self.emit_conda_activate_cmds()} conda install {options} --prefix={self.env_loc} {package} -y',
                check=True, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
            return not bool(p.returncode)
        except Exception as e:
            logging.debug(f'Can\'t install the conda package {package} for the environment {self.env_name}.')
            logging.error(f"Error: {e}")
            return False

    def download_pip(self, package, options=""):
        """
        Download a pip package in the conda environment.
        :param package: package or the absolute path of the requirements file (.txt) to download
        :type package: str
        :param options: pip options for the download
        :type options: str
        """

        if '.txt' in package:
            package = f' -r {package}'

        os.makedirs(self.download_directory, exist_ok=True)

        try:
            logging.debug(f'Download the pip package {package} for the environment {self.env_name}.')
            p = subprocess.run(
                f'{self.emit_conda_activate_cmds()} pip download -d {self.download_directory} {options} {package}',
                shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
            return not bool(p.returncode)
        except Exception as e:
            logging.debug(f'Can\'t download the pip package {package} for the environment {self.env_name}.')
            logging.error(f"Error: {e}")
            return False

    def install_pip(self, package, local=False, options=""):
        """
        Install a pip package in the conda environment.
        :param package: package or the absolute path of the requirements file (.txt) to download
        :type package: str
        :param local: if the package is local (otherwise download the package)
        :type local: bool
        :param options: pip options for the installation
        :type options: str
        """
        p = subprocess.run('pwd', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        if '.txt' in package:
            package = f' -r {package}'
        if local:
            options += f' --no-index --find-links={self.download_directory}'
            os.makedirs(self.download_directory, exist_ok=True)

        try:
            logging.debug(f'Install the pip package {package} for the environment {self.env_name}.')
            p = subprocess.run(f'{self.emit_conda_activate_cmds()} pip install {options} {package}',
                               shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
            return not bool(p.returncode)
        except Exception as e:
            logging.debug(f'Can\'t install the pip package {package} for the environment {self.env_name}:')
            logging.error(f"Error: {e}")
            return False

    def purge_cache_pip(self):
        """
        Purge the cache for downloaded packages in the conda environment.
        """
        try:
            logging.debug(f'Purge the empty cache folder for downloaded packages in the environment {self.env_name}.')
            os.rmdir(self.download_directory)
            return True
        except:
            logging.debug(f'Purge the cache non empty for downloaded packages in the environment {self.env_name}.')
            try:
                for root, dirs, files in os.walk(self.download_directory, topdown=False):
                    for name in files:
                        os.remove(os.path.join(root, name))
                    for name in dirs:
                        os.rmdir(os.path.join(root, name))
                os.rmdir(self.download_directory)
                return True
            except OSError as e:
                logging.debug(f'Can\'t purge the cache for downloaded packages in the environment {self.env_name}.')
                logging.error(f'Error: {e}')
                return False

    def get_env_loc(self):
        """
        Getter for the location of the conda environment. 
        """
        return self.env_loc
