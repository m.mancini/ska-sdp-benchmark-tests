from typing import Callable, Tuple

import reframe as rfm
from reframe.core.builtins import run_before, run_after


class BenchmarkBaseMixin(rfm.RegressionMixin):
    """Base mixin for all Regression Tests.
    Uses the :class:`rfm.RegressionMixin` as basis. This provides access to all ReFrame pipeline hooks.

    This mixin provides a list of prerun and a list of postrun checks.
    Checks can be added by the developer using `self.add_prerun_check(f)` and `self.add_postrun_check(f)` respectively.
    `f` needs to be a callable that requires no additional argument and returns either a `Tuple[bool, str]`
    or an exception. In the first case, the boolean reports on the predicate status (True if the check completed
    successfully, False otherwise). The string part is only used in case the predicate fails and reports
    to the user what exactly failed. If an exception is thrown while calling `f`, the predicate is defined to be
    `False` with the exception message being used as message string."""

    prerun_checks = None
    postrun_checks = None

    def __init__(self):
        """Initializes the pre- and postrun checks with an empty list.
        This is necessary because otherwise the list instances are shared across test instances."""
        super().__init__()
        self.prerun_checks = []
        self.postrun_checks = []

    def add_prerun_check(self, predicate: Callable[[], Tuple[bool, str]]):
        """Adds a prerun check to the prerun check list.

        :arg predicate: The predicate to be called. Returns either Tuple[bool, str] or an exception when called
        """
        self.prerun_checks.append(predicate)

    def add_postrun_checks(self, predicate: Callable[[], Tuple[bool, str]]):
        """Adds a postrun check to the postrun check list.

        :arg predicate: The predicate to be called. Returns either Tuple[bool, str] or an exception when called
        """
        self.postrun_checks.append(predicate)

    @run_after('setup')
    def run_prerun_checks(self):
        """Executes the list of prerun predicates. If any fail the test is skipped.
        Each failing test is reported to the user as a message using self.skip().
        """
        if self.prerun_checks is None:
            return

        msgs = []
        for i, pred in enumerate(self.prerun_checks):
            # run predicate
            try:
                res, msg = pred()
            except Exception as e:
                res = None
                msg = f"Exception: {e}"

            if not res:
                msgs.append(f"Check {i + 1} failed: {msg}")
        if len(msgs) > 0:
            nl = "\n>> "
            self.skip(f"{len(msgs)}/{len(self.prerun_checks)} prerun checks failed. Messages:\n>> {nl.join(msgs)}")

    @run_before('cleanup')
    def run_postrun_checks(self):
        """Executes the list of postrun predicates. If any fail the test is marked as failed.
        Each failing test is reported to the user as a message using a RuntimeError.
        """
        if self.postrun_checks is None:
            return

        msgs = []
        for i, pred in enumerate(self.postrun_checks):
            # run predicate
            try:
                res, msg = pred()
            except Exception as e:
                res = None
                msg = f"Exception: {e}"

            if not res:
                msgs.append(f"Check {i + 1} failed: {msg}")
        if len(msgs) > 0:
            nl = "\n>> "
            raise RuntimeError(f"{len(msgs)}/{len(self.postrun_checks)} postrun checks failed. "
                               f"Marking test as failed. "
                               f"Messages:\n>> {nl.join(msgs)}")


class RunOnlyBenchmarkBase(rfm.RunOnlyRegressionTest, BenchmarkBaseMixin):
    """Base class for RunOnly Regression Tests.
    Uses the :class:`BenchmarkBaseMixin` to mix into the :class:`rfm.RunOnlyRegressionTest` class.
    This is used if a test only needs to run an application without needing to compile anything before."""
    def __init__(self):
        """__init__ needs to call the Mixin-Init explicitly, otherwise it does not get called at all."""
        BenchmarkBaseMixin.__init__(self)
        super().__init__()


class CompileOnlyBenchmarkBase(rfm.CompileOnlyRegressionTest, BenchmarkBaseMixin):
    """Base class for CompileOnly Regression Tests.
    Uses the :class:`BenchmarkBaseMixin` to mix into the :class:`rfm.CompileOnlyRegressionTest` class.
    This is used if a test only needs to compile sources without the usual run phases."""
    def __init__(self):
        """__init__ needs to call the Mixin-Init explicitly, otherwise it does not get called at all."""
        BenchmarkBaseMixin.__init__(self)
        super().__init__()


class BenchmarkBase(rfm.RegressionTest, BenchmarkBaseMixin):
    """Base class for full Regression Tests.
    Uses the :class:`BenchmarkBaseMixin` to mix into the :class:`rfm.RegressionTest` class.
    This is used if a test needs to compile sources and then execute an usual run regression test."""
    def __init__(self):
        """__init__ needs to call the Mixin-Init explicitly, otherwise it does not get called at all."""
        BenchmarkBaseMixin.__init__(self)
        super().__init__()
